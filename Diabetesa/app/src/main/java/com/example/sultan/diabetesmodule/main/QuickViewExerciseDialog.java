package com.example.sultan.diabetesmodule.main;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.sultan.diabetesmodule.R;
import com.example.sultan.diabetesmodule.start_workout.StartWorkout;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class QuickViewExerciseDialog extends DialogFragment implements View.OnClickListener {


    private RecyclerView recyclerView;
    private QuickViewMachineRecyclerAdapter adapter;
    ArrayList<Boolean> machineCompletedList;
    List<ParseObject> workouts;
    TextView cancel;
    TextView startWorkout;
    String objectId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_quick_view_workout, container, false);

        getDialog().setTitle("Quick View Workout");

        cancel = (TextView) view.findViewById(R.id.cancel);
        startWorkout = (TextView) view.findViewById(R.id.yes);

        recyclerView = (RecyclerView) view.findViewById(R.id.workoutRecyclerView);
        adapter = new QuickViewMachineRecyclerAdapter(getActivity(), getData());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        cancel.setOnClickListener(this);
        startWorkout.setOnClickListener(this);

        objectId = getArguments().getString("userId");

        ParseQuery<ParseObject> query = ParseQuery.getQuery("UserWorkout");
        query.getInBackground(objectId, new GetCallback<ParseObject>() {
            public void done(ParseObject workout, ParseException e) {
                if (e == null) {

                    //android.util.Log.d("QUERY", workout.get("machine").toString());

                    ArrayList<String> names = (ArrayList<String>) workout.get("machines");
                    ArrayList<String> sets = (ArrayList<String>) workout.get("sets");
                    machineCompletedList = new ArrayList<Boolean>(names.size());



                    for (int i = 0; i < names.size(); i++) {
                        machineCompletedList.add(i, false);
                        String name = names.get(i);
                        ArrayList<String> set = new ArrayList<String>(Arrays.asList(sets.get(i).split(" ")));
                        adapter.addRow(new QuickViewMachineRecyclerInfo(name, set));
                    }


                } else {
                    // something went wrong
                    Log.d("ERROR", e.toString());
                }
            }
        });


        return view;
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.cancel) {
            dismiss();
        } else if (view.getId() == R.id.startWorkout) {
            dismiss();

            Intent intent = new Intent(getActivity(), StartWorkout.class);
            intent.putExtra("selectedId", objectId);
            intent.putExtra("completedList", machineCompletedList);
            QuickViewExerciseDialog.this.startActivity(intent);

        }

    }

    public static QuickViewExerciseDialog newInstance(String userId) {
        QuickViewExerciseDialog f = new QuickViewExerciseDialog();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString("userId", userId);
        f.setArguments(args);

        return f;
    }

    // Method for getting data from list of images
    public static List<QuickViewMachineRecyclerInfo> getData() {
        List<QuickViewMachineRecyclerInfo> data = new ArrayList<>();

        String[] sets = {};


        for (int i = 0; i < sets.length; i++) {
            QuickViewMachineRecyclerInfo current = new QuickViewMachineRecyclerInfo();
            data.add(current);
        }

        return data;
    }


    //=================================== View Workout Recycler Adapter =========================================================

    // the <> contains my custom viewholder; MyViewHolder, and this class will return MyViewHolder as well
    public static class QuickViewMachineRecyclerAdapter extends RecyclerView.Adapter<QuickViewMachineRecyclerAdapter.MyViewHolder> {

        // emptyList takes care of null pointer exception
        List<QuickViewMachineRecyclerInfo> data = Collections.emptyList();
        LayoutInflater inflator;

        public QuickViewMachineRecyclerAdapter(Context context, List<QuickViewMachineRecyclerInfo> data) {
            inflator = LayoutInflater.from(context);
            this.data = data;
        }

        public void addRow(QuickViewMachineRecyclerInfo row) {
            data.add(row);
            notifyItemInserted(getItemCount() - 1);
        }

        // Called when the recycler view needs to create a new row
        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = inflator.inflate(R.layout.row_quick_view_workout, parent, false);
            MyViewHolder holder = new MyViewHolder(view, new MyViewHolder.TimelineMachineInterface() {
                public void onPotato(View caller, int position) {
                    android.util.Log.d("onPatato", "Poh-tah-tos " + position);

                }

                public void onTomato(ImageView callerImage, int position) {
                    android.util.Log.d("onTOmato", "To-m8-tohs");

                }
            });
            return holder;
        }

        // Setting up the data for each row
        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {

            // This gives us current information list object
            QuickViewMachineRecyclerInfo current = data.get(position);

            //Log.d("QUICK VIEW BIND MYREP #", String.valueOf(myReps.size()));
            holder.machineName.setText(current.getMachineName());

            //holder.sets.get(0).setVisibility(View.GONE);
            for(int i=0; i < holder.sets.size(); i++){
                if(current.getSet().size() < i+1){
                    holder.sets.get(i).setVisibility(View.GONE);
                } else {
                    holder.sets.get(i).setText(current.getSet().get(i));
                }

            }

            //holder.timerAmount.setText(current.getTimer());
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        // Created my custom view holder
        public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            TextView machineName;
            ArrayList<TextView> sets;
            //ImageView excerciseCompleted;
            public TimelineMachineInterface mListener;

            // itemView will be my own custom layout View of the row
            public MyViewHolder(View itemView, TimelineMachineInterface listener) {
                super(itemView);

                mListener = listener;

                LinearLayout ll = (LinearLayout) itemView.findViewById(R.id.setsLayout);
                //Link the objects
                machineName = (TextView) itemView.findViewById(R.id.machineName);
                sets = new ArrayList<TextView>(15);
                int numSets =15;
                for(int i=0; i < numSets; i++){
                    TextView tv = new TextView(itemView.getContext());
                    final float scale = itemView.getContext().getResources().getDisplayMetrics().density;
                    int pixels = (int) (30 * scale + 0.5f);
                    LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(pixels,pixels);

                    pixels = (int) (4 * scale + 0.5f);
                    llp.setMargins(pixels, 0, 0, 0);

                    tv.setLayoutParams(llp);
                    tv.setText("N");
                    tv.setGravity(Gravity.CENTER);
                    tv.setTextAppearance(itemView.getContext(), R.style.quickViewStyle);
                    tv.setBackgroundResource(R.drawable.black_ring);
                    ll.addView(tv);
                    sets.add(tv);
                }

                // excerciseCompleted = (ImageView) itemView.findViewById(R.id.excerciseCheckmark);

                //machineName.setOnClickListener(this);
                itemView.setOnClickListener(this);
                //excerciseCompleted.setOnClickListener(this);


            }

            @Override
            public void onClick(View v) {
                if (v instanceof ImageView) {


                } else {
                    mListener.onPotato(v, getAdapterPosition());
                }
            }

            public interface TimelineMachineInterface {
                public void onPotato(View caller, int position);

                public void onTomato(ImageView callerImage, int position);
            }
        }


    }

    //===========================================================================================================


    //=================================== View Workout Recycler Info Class =====================================
    public static class QuickViewMachineRecyclerInfo {


        private String machineName;
        private ArrayList<String> sets;
        //private String timerAmount;

        public QuickViewMachineRecyclerInfo() {
            super();

        }

        public QuickViewMachineRecyclerInfo(String name, ArrayList<String> sets) {
            super();
            this.machineName = name;
            this.sets = sets;
        }

        public String getMachineName() {
            return machineName;
        }

        public void setMachineName(String machineName) {
            this.machineName = machineName;
        }

        public ArrayList<String> getSet() {
            return sets;
        }

        public void setSet(ArrayList<String> sets) {
            this.sets = sets;
        }
/*
    public String getTimer() {
        return timerAmount;
    }
    public void setTimer(String timerAmount) {
        this.timerAmount = timerAmount;
    }

*/
    }


    //===========================================================================================================

}
