package com.example.sultan.diabetesmodule.log;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sultan.diabetesmodule.R;

import java.util.Collections;
import java.util.List;


// the <> contains my custom viewholder; MyViewHolder, and this class will return MyViewHolder as well
public class LogRecyclerAdapter extends RecyclerView.Adapter<LogRecyclerAdapter.MyViewHolder>{

    // emptyList takes care of null pointer exception
    List<LogRecyclerInfo> data = Collections.emptyList();
    LayoutInflater inflator;

    public LogRecyclerAdapter(Context context, List<LogRecyclerInfo> data){
        inflator = LayoutInflater.from(context);
        this.data = data;
    }

    // Called when the recycler view needs to create a new row
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflator.inflate(R.layout.row_log, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    // Setting up the data for each row
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        // This gives us current information list object
        LogRecyclerInfo current = data.get(position);

        holder.time.setText(current.logTime);
        holder.image.setImageResource(current.logIconId);
        holder.glucose.setText(String.valueOf(current.glucose));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    // Created my custom view holder
    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView time;
        TextView glucose;
        ImageView image;



        // itemView will be my own custom layout View of the row
        public MyViewHolder(View itemView) {
            super(itemView);

            //Link the objects
            time = (TextView) itemView.findViewById(R.id.logTime);
            glucose = (TextView) itemView.findViewById(R.id.glucoseSheild);
            image = (ImageView) itemView.findViewById(R.id.logIcon);
        }
    }
}