package com.example.sultan.diabetesmodule;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;


// the <> contains my custom viewholder; MyViewHolder, and this class will return MyViewHolder as well
public class SetRecyclerAdapter extends RecyclerView.Adapter<SetRecyclerAdapter.MyViewHolder>{

    // emptyList takes care of null pointer exception
    List<SetRecyclerInfo> data = Collections.emptyList();
    LayoutInflater inflator;

    public SetRecyclerAdapter(Context context, List<SetRecyclerInfo> data){
        inflator = LayoutInflater.from(context);
        this.data = data;
    }

    // Called when the recycler view needs to create a new row
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflator.inflate(R.layout.row_set, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    // Setting up the data for each row
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        // This gives us current information list object
        SetRecyclerInfo current = data.get(position);

        holder.setAmount.setText(current.getSet());
        holder.repsAmount.setText(current.getRep());
        holder.currentSet.setVisibility(View.INVISIBLE);
    }

    public void addRow(SetRecyclerInfo row){
        data.add(row);
        notifyItemInserted(getItemCount() - 1);
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    // Created my custom view holder
    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView setAmount;
        TextView repsAmount;
        TextView currentSet;

        // itemView will be my own custom layout View of the row
        public MyViewHolder(View itemView) {
            super(itemView);

            //Link the objects
            setAmount = (TextView) itemView.findViewById(R.id.setAmount);
            repsAmount = (TextView) itemView.findViewById(R.id.repAmount);
            currentSet = (TextView) itemView.findViewById(R.id.current);
        }
    }
}