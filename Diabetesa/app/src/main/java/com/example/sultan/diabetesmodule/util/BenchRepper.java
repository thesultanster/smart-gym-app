package com.example.sultan.diabetesmodule.util;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.util.Log;

/** BenchRepper
 *      Description: Detects vertical linear movement with device's screen parallel to surface and
 *          counts rep (used for bench press)
 *      Usage: You need to override "Rep" function and write the following code:
 *          @Override
            public void rep()
            {
                if( incAllowed ) {
                    repCountTV.setText((++repCount) + "");
                    incAllowed = false;
                    }

                final ScheduledExecutorService worker = Executors.newSingleThreadScheduledExecutor();

                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        incAllowed = true;
                    }
                };

                worker.schedule(r,1500, TimeUnit.MILLISECONDS);
            }
 */
public class BenchRepper
{
    final static String TAG = "SPEED: ";
    //PRIVATE VARIABLES [START] --------------------------------------------------------------------
    //Context Variable
    Context context;

    //Sensor Variables
    SensorManager sensorManager;
    Sensor sensor;
    SensorEventListener sensorEventListener;

    //Computation Variables
    float x, y, z;
    float speed;
    float lastSpeed;
    long lastUpdate = 0;
    int REP_THRESH = 10;

    //Public Variables
    boolean isRunning;
    boolean startedRep;


    //New thread for updating values
    Thread thread;
    Handler handler;
    //[END] ----------------------------------------------------------------------------------------



    //CONSTRUCTOR [START] --------------------------------------------------------------------------
    public BenchRepper(Context c)
    {
        context = c;
        initialize();
    }
    //[END] ----------------------------------------------------------------------------------------


    //UTILITY FUNCTIONS [START] --------------------------------------------------------------------
    public void start()
    {
        isRunning = true;
        thread.run();
        sensorManager.registerListener(sensorEventListener, sensor, SensorManager.SENSOR_DELAY_GAME);
    }

    public void end()
    {
        isRunning = false;
        sensorManager.unregisterListener(sensorEventListener, sensor);
    }
    //[END] ----------------------------------------------------------------------------------------



    //HELPER FUNCTIONS [START] ---------------------------------------------------------------------
    void initialize()
    {
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        sensorEventListener = new SensorEventListener()
        {
            @Override
            public void onSensorChanged(SensorEvent event)
            {
                x = event.values[0];
                y = event.values[1];
                z = event.values[2];
                //z = z < 0 ? z + 9.8f : z - 9.8f;

                long currentTime = System.currentTimeMillis();
                if(currentTime - lastUpdate > 100)
                {
                    long differenceTime = currentTime - lastUpdate;
                    lastUpdate = currentTime;

                    //float speed = Math.abs(z - last_z)/ differenceTime * 10000;
                    speed = Math.abs((z) /differenceTime*1000);
                    speed = z < 0 ? speed * (-1) : speed;

                    Log.d(TAG, speed + "");

                    if(Math.abs(speed) > REP_THRESH && Math.abs(x/differenceTime*1000) < 10 && Math.abs(y/differenceTime*1000) < 10)
                        lastSpeed = startedRep ? lastSpeed : speed;
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };

        //create new thread to constantly update
        thread = new Thread()
        {
            public void run()
            {
                if(isRunning)
                {
                    update();
                    handler.postDelayed(this, 50);
                }
            }
        };

        //handler for new thread
        handler = new Handler();

        //rep is not started yet
        startedRep = false;
    }
    //[END] ----------------------------------------------------------------------------------------


    //HELPER FUNCTIONS [START] ---------------------------------------------------------------------
    // updates accelerometer values
    void update()
    {
        //check the altitude when phone is faced down or up
        // check:    up                    down
        if(lastSpeed > REP_THRESH && speed < REP_THRESH)
        {
            Log.d(TAG,"UP");

            //reset variables
            startedRep = false; //finished rep
            lastSpeed = 0;
            speed = 0;

            //notify
            rep();
        }
    }

    // Notify to MainActivity that a rep has been finished
    void rep()
    {
        //user will have to implement by overriding
    }
    //[END] ----------------------------------------------------------------------------------------

}
