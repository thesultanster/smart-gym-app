package com.example.sultan.diabetesmodule.start_workout;

import com.parse.ParseUser;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by Sultan on 8/26/2015.
 */
public class Client implements Runnable{

    private Socket socket;//SOCKET INSTANCE VARIABLE
    String HOST;
    int PORT;
    String machineId;

    public Client(String HOST, int PORT, String machineId) throws IOException {

        this.HOST = HOST;
        this.PORT = PORT;
        this.machineId = machineId;

    }

    @Override
    public void run() //(IMPLEMENTED FROM THE RUNNABLE INTERFACE)
    {
        try //HAVE TO HAVE THIS FOR THE in AND out VARIABLES
        {
            socket = new Socket(HOST, PORT);//CONNECT TO THE SERVER

            PrintWriter out = new PrintWriter(socket.getOutputStream());//GET THE SOCKETS OUTPUT STREAM (THE STREAM YOU WILL SEND INFORMATION TO THEM FROM)

            out.println(ParseUser.getCurrentUser().getObjectId() + "_" + machineId);//RESEND IT TO THE CLIENT
            out.flush();//FLUSH THE STREAM

            android.util.Log.d("NFC SENT STRING",ParseUser.getCurrentUser().getObjectId() + "_" + machineId);

        }
        catch (Exception e)
        {
            android.util.Log.d("NFC STRING NOT SENT",ParseUser.getCurrentUser().getObjectId() + "_" + machineId);
            e.printStackTrace();//MOST LIKELY THERE WONT BE AN ERROR BUT ITS GOOD TO CATCH
        }
    }

    void close() throws IOException {
        socket.close();
    }

}