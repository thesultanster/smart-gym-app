package com.example.sultan.diabetesmodule.start_workout;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.sultan.diabetesmodule.start_workout.MachineProgressFragment;

import java.util.ArrayList;


public class MachineProgressPagerAdapter extends FragmentStatePagerAdapter {

    private Context context;
    ArrayList<String> names = new ArrayList<String>();
    ArrayList<String> sets = new ArrayList<String>();

    public MachineProgressPagerAdapter(FragmentManager fm, Context context, ArrayList<String> names, ArrayList<String> sets) {
        super(fm);
        this.context = context;
        this.names = names;
        this.sets = sets;
    }

    @Override
    public Fragment getItem(int position) {
        //android.util.Log.d("GET ITEM POSITION", String.valueOf(position));
        MachineProgressFragment machineProgressFragment = MachineProgressFragment.getInstance(position, this.sets, this.names.get(position));
        return machineProgressFragment;
        //return new MachineProgressFragment();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return names.get(position);

    }

    @Override
    public int getCount() {
        if (names == null)
            return 0;
        return names.size();
    }


}

