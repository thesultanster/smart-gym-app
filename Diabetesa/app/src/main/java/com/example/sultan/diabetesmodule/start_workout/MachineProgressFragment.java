package com.example.sultan.diabetesmodule.start_workout;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.TextView;

import com.example.sultan.diabetesmodule.util.Exercise;
import com.example.sultan.diabetesmodule.util.Leveler;
import com.example.sultan.diabetesmodule.R;
import com.example.sultan.diabetesmodule.util.SerializeObject;
import com.example.sultan.diabetesmodule.SetRecyclerAdapter;
import com.example.sultan.diabetesmodule.SetRecyclerInfo;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ValueFormatter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MachineProgressFragment extends Fragment  {

    private RecyclerView recyclerView;
    private SetRecyclerAdapter adapter;
    ArrayList<String> sets;
    int position;
    String exerciseName;
    Leveler rightLeveler;
    Leveler leftLeveler;
    BarChart chart;
    TextView sidewaysCurrentRep;
    TextView sidewaysCurrentSet;
    View myLayout;
    GridLayout sidewaysProgress;
    int rep = 0;
    int set = 0;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_machine_progress, container, false);
        Bundle bundle = getArguments();
        if (bundle != null) {
            // Set values
            sets = (ArrayList<String>) bundle.getSerializable("sets");
            position = bundle.getInt("position");
            exerciseName = bundle.getString("exerciseName");
            chart = (BarChart) layout.findViewById(R.id.chart);
        }

        rightLeveler = (Leveler) layout.findViewById(R.id.rightLeveler);
        leftLeveler = (Leveler) layout.findViewById(R.id.leftLeveler);
        sidewaysCurrentRep = (TextView) layout.findViewById(R.id.sidewaysCurrentRep);
        sidewaysCurrentSet = (TextView) layout.findViewById(R.id.sidewaysCurrentSet);

        sidewaysProgress = (GridLayout) layout.findViewById(R.id.sidewaysProgress);


        recyclerView = (RecyclerView) layout.findViewById(R.id.setRecyclerView);
        adapter = new SetRecyclerAdapter(getActivity(), getData(sets, position));
        //recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        setupChart(exerciseName, layout);

        if (exerciseName.equals("Bench Press")) {
            //rightLeveler.setVisibility(View.VISIBLE);
            //sidewaysProgress.setVisibility(View.VISIBLE);
            //leftLeveler.setVisibility(View.VISIBLE);
            //chart.setVisibility(View.INVISIBLE);
            //rightLeveler.start();
            //leftLeveler.start();
        }
        else{
            //sidewaysProgress.setVisibility(View.INVISIBLE);
            //rightLeveler.setVisibility(View.GONE);
            //leftLeveler.setVisibility(View.GONE);
            //rightLeveler.end();
            //leftLeveler.end();
        }


        myLayout = layout;
        return layout;
    }


    public static MachineProgressFragment getInstance(int position, ArrayList<String> sets, String exerciseName) {
        MachineProgressFragment machineProgressFragment = new MachineProgressFragment();

        //Log.d("GET INSTANCE POSITION", String.valueOf(position));

        Bundle args = new Bundle();
        args.putInt("position", position);
        args.putSerializable("sets", sets);
        args.putString("exerciseName", exerciseName);
        machineProgressFragment.setArguments(args);

        return machineProgressFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    // Method for getting data from list of images
    public static List<SetRecyclerInfo> getData(ArrayList<String> sets, int position) {
        List<SetRecyclerInfo> data = new ArrayList<>();

        List<String> myReps = new ArrayList<String>(Arrays.asList(sets.get(position).split(" ")));
        for (int i = 0; i < myReps.size(); i++) {
            SetRecyclerInfo current = new SetRecyclerInfo();
            current.setSet(String.valueOf(i));
            current.setRep(myReps.get(i));
            //current.setTimer(timers[i]);
            data.add(current);
        }

        return data;
    }


    void setupChart(String exerciseName, View layout) {

        ArrayList<BarEntry> oldReps = new ArrayList<BarEntry>();
        // Array of Lines
        ArrayList<BarDataSet> lines = new ArrayList<BarDataSet>();

        int largestNumber = 0;

        // Populate oldreps dataset
        //*********************************************************************************************************************
        ArrayList<Integer> previousRepList;

        // READ REPS
        String serRead = SerializeObject.ReadSettings(getContext(), exerciseName + ".dat");
        if (serRead != null && !serRead.equalsIgnoreCase("")) {
            Object obj = SerializeObject.stringToObject(serRead);
            // Then cast it to your object and
            if (obj instanceof Exercise) {
                // Create old Exercise
                Exercise oldExercise = (Exercise) obj;

                // Get previous rep list
                previousRepList = oldExercise.repList;

                largestNumber = oldExercise.repList.size();


                for (int i = 0; i < oldExercise.repList.size(); i++) {
                    oldReps.add(new BarEntry(previousRepList.get(i), i));
                }


                //Line info
                BarDataSet oldSet = new BarDataSet(oldReps, "Last Exercise");
                oldSet.setDrawValues(false);
                oldSet.setHighLightColor(Color.parseColor("#00A8A4"));
                oldSet.setColor(Color.parseColor("#00A8A4"));
                oldSet.setBarSpacePercent(75);

                lines.add(oldSet);


            }
        }


        //*********************************************************************************************************************

        // Create X axis based on largest x value + 2
        // Add 1 to accomodate the first set taking two points
        String[] xAxis = new String[largestNumber + 1];
        for (int i = 0; i < xAxis.length; i++) {
            xAxis[i] = String.valueOf(i+1);
        }


        // Set to chart
        BarData data = new BarData(xAxis, lines);
        chart.setData(data);


        // no description text
        chart.setDescription("");
        chart.setNoDataTextDescription("There's no data. Exercise now!");

        // enable / disable grid background
        chart.setDrawGridBackground(false);

        // enable touch gestures
        chart.setTouchEnabled(false);

        // enable scaling and dragging
        chart.setDragEnabled(false);
        chart.setScaleEnabled(false);

        // if disabled, scaling can be done on x- and y-axis separately
        chart.setPinchZoom(false);
        //chart.setBackgroundColor(Color.WHITE);

        // get the legend (only possible after setting data)
        Legend l = chart.getLegend();
        l.setEnabled(true);

        chart.getAxisLeft().setEnabled(true);
        chart.getAxisRight().setEnabled(false);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.getXAxis().setGridColor(Color.WHITE);

        chart.getAxisLeft().setLabelCount(4, true);
        chart.getAxisLeft().setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return String.valueOf((int) (value + 0.5));
            }
        });
    }

    void incrementRep(){
      // View layout = inflater.inflate(R.layout.fragment_machine_progress, container, false);

       rep++;
        //sidewaysCurrentRep = (TextView) myLayout.findViewById(R.id.sidewaysCurrentRep);
       sidewaysCurrentRep.setText(String.valueOf(rep));
   }

    void incrementSet(){
        // View layout = inflater.inflate(R.layout.fragment_machine_progress, container, false);

        rep = 0;
        sidewaysCurrentRep.setText(String.valueOf(rep));

        set++;
        //sidewaysCurrentSet = (TextView) myLayout.findViewById(R.id.sidewaysCurrentSet);
        sidewaysCurrentSet.setText(String.valueOf(set));
    }
}
