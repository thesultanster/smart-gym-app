package com.example.sultan.diabetesmodule.util;

import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;

import com.example.sultan.diabetesmodule.OrientationProvider;

/** Leveler
 *      Description: displays a gauge with red needle which shows the balance of the device
 *      Usage: Instantiate object and call start() and stop() whenever suitable
 *      Sultan: Khan
 */
public class Leveler extends View
{
    //PRIVATE VARIABLES [START] --------------------------------------------------------------------
    //Fixed Constant Variables
    final static double maxAngle = 180;
    final static double minAngle = 0;

    //Background Variables
    int width;
    int height;

    //Canvas Variables
    Canvas canvas;

    //Paint Variables
    Paint needlePaint;
    Paint tickPaint;
    Paint safeZonePaint;

    //Computation Variable
    double currentAngle;

    //Orientation Variables
    OrientationProvider orientationProvider;

    //New thread for updating values
    Thread thread;
    Handler handler;
    // [END] ---------------------------------------------------------------------------------------



    //OVERRIDE FUNCTIONS [START] -------------------------------------------------------------------
    @Override
    protected void onDraw(Canvas canvas)
    {
        if(canvas != null)
        {
            this.canvas = canvas;

            //draw background
            drawBackground();

            //draw needle
            drawNeedle();

            canvas.restore();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int desiredWidth = 100;
        int desiredHeight = 100;

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        //Measure Width
        if (widthMode == MeasureSpec.EXACTLY) {
            //Must be this size
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            width = Math.min(desiredWidth, widthSize);
        } else {
            //Be whatever you want
            width = desiredWidth;
        }

        //Measure Height
        if (heightMode == MeasureSpec.EXACTLY) {
            //Must be this size
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            height = Math.min(desiredHeight, heightSize);
        } else {
            //Be whatever you want
            height = desiredHeight;
        }

        //MUST CALL THIS
        setMeasuredDimension(width, height);
    }
    //[END] ----------------------------------------------------------------------------------------



    //CONSTRUCTORS [START] -------------------------------------------------------------------------
    public Leveler(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        initialize();
    }
    //[END] ----------------------------------------------------------------------------------------



    //UTILITY FUNCTIONS [START] --------------------------------------------------------------------
    //start the leveler
    public void start()
    {
        orientationProvider.start();
        thread.run();
    }

    public void end(){ orientationProvider.end(); }


    //stop the leveler
    //[END] ----------------------------------------------------------------------------------------



    //HELPER FUNCTIONS [START] ---------------------------------------------------------------------
    void initialize()
    {
        //set view's background
        //setBackgroundColor(0x00FFFFFF); //fully transparent
        setBackgroundColor(0xFFFFFFFF); //white

        //Paint
        needlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        needlePaint.setStrokeWidth(10.0f);
        needlePaint.setStyle(Paint.Style.STROKE);
        needlePaint.setColor(Color.rgb(255, 0, 0));

        tickPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        tickPaint.setStrokeWidth(3.0f);
        tickPaint.setStyle(Paint.Style.STROKE);
        tickPaint.setColor(Color.rgb(0, 0, 0));

        safeZonePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        safeZonePaint.setStrokeWidth(3.0f);
        safeZonePaint.setStyle(Paint.Style.FILL);
        safeZonePaint.setColor(Color.rgb(189, 195, 199));

        //Computation
        currentAngle = 0.0;

        //Orientation Provider
        orientationProvider = new OrientationProvider(getContext());

        //create new thread to constantly update
        thread = new Thread() {
            public void run() {
                if (orientationProvider.isRunning) {
                    update();
                    handler.postDelayed(this, 50);
                }
            }
        };

        //handler for new thread
        handler = new Handler();
    }

    //draw the tick
    void drawNeedle()
    {
        //compute fraction
        double fraction = currentAngle / maxAngle;

        //coordinates
        int x1 = 0;
        int y1 = (int)(fraction*height);
        int x2 = x1 + width;
        int y2 = y1;
        canvas.drawLine(x1, y1, x2, y2, needlePaint);
    }

    //animate the tick move (move tick from currentAngle to newAngle)
    @TargetApi(11)
    ValueAnimator moveTick(double newAngle, long duration, long startDelay)
    {
        double convertedAngle = convertAngle(newAngle);

        //restrict the bounds
        if (convertedAngle < minAngle)
            convertedAngle = minAngle;

        if (convertedAngle > maxAngle)
            convertedAngle = maxAngle;

        ValueAnimator va = ValueAnimator.ofObject(new TypeEvaluator<Double>()
        {
            @Override
            public Double evaluate(float fraction, Double startValue, Double endValue)
            {
                return currentAngle + fraction * (endValue - currentAngle);
            }
        }, currentAngle, convertedAngle);

        va.setDuration(duration);
        va.setStartDelay(startDelay);
        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                Double value = (Double) animation.getAnimatedValue();
                if (value != null)
                    setAngle(value);
            }
        });
        va.start();

        return va;
    }

    // set angle
    void setAngle(double convertedAngle)
    {
        if (convertedAngle < minAngle)
            currentAngle = minAngle;
        if (convertedAngle > maxAngle)
            currentAngle = maxAngle;
        currentAngle = convertedAngle;
        invalidate(); //calls onDraw (to update the needle position)
    }

    //convert it to all positive angle [0 - 180] from [-90 - 90]
    double convertAngle(double CA)
    {
        double converted;

        if(CA < 0)
            converted = 90 + (-1 * CA); //upper half
        else if(CA > 0)
            converted = 90 - CA; //lower half
        else //CA == 0
            converted = 90; //exactly at middle

        return converted;
    }

    //draw background for the gauge
    void drawBackground()
    {
        //clear canvas
        canvas.drawColor(0xFFecf0f1);

        //set green safe zone
        canvas.drawRect(0, (height/5)*2 + 30, width, (height/5)*3 - 30, safeZonePaint);

        //horizontal buffers
        final int hShortBuffer = width/3;
        //final int hLongBuffer = width/5;

        //vertical buffers
        final int vBuffer = (height/2)/4;

        //coordinate variables
        float x1, y1, x2, y2;

        //draw ticks
        x1 = hShortBuffer;
        x2 = width - hShortBuffer;
        y1 = y2 = vBuffer;
        for(int i = 1; y1 < height; ++i)
        {
            y1 = y2 = i*vBuffer;
            if(Math.abs(y1 - height/2) < 10)
                continue;
            canvas.drawLine(x1, y1, x2, y2, tickPaint);
        }
    }

    // updates gyroscope values
    void update()
    {
        float[] orientationV = orientationProvider.getOrientationVector();
        moveTick(orientationV[1], 50, 0);
    }
    //[END] ----------------------------------------------------------------------------------------
}
