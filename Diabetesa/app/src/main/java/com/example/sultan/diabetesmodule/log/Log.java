package com.example.sultan.diabetesmodule.log;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sultan.diabetesmodule.Login;
import com.example.sultan.diabetesmodule.NavigationDrawerFragment;
import com.example.sultan.diabetesmodule.main.QuickViewExerciseDialog;
import com.example.sultan.diabetesmodule.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class Log extends ActionBarActivity {

    private android.support.v7.widget.Toolbar toolbar;

    private RecyclerView recyclerView;
    private TimelineRecyclerAdapter adapter;
    List<ParseObject> timelines;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);

        Intent intent;
        if(ParseUser.getCurrentUser() == null){
            intent = new Intent(this, Login.class);
            startActivity(intent);
        }

        ParseUser parseUser = ParseUser.getCurrentUser();

        // Action Bar
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // Navigation Drawer
        NavigationDrawerFragment drawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);

        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);




        FragmentManager fragmentManager = getFragmentManager();

        recyclerView = (RecyclerView) findViewById(R.id.yourTimelineRecyclerView);
        adapter = new TimelineRecyclerAdapter(Log.this, getData(), fragmentManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(Log.this));

        ParseQuery<ParseObject> query = ParseQuery.getQuery("WorkoutSession");
        query.whereEqualTo("userId", ParseUser.getCurrentUser().getObjectId());
        query.orderByDescending("createdAt");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> timelineNames, ParseException e) {
                if (e == null) {
                    android.util.Log.d("WorkoutSession", String.valueOf(timelineNames.size()));

                    timelines = timelineNames;
                    for (ParseObject name : timelineNames) {
                        adapter.addRow(new TimelineRecyclerInfo(name.getString("name"), name));
                    }

                } else {
                    Toast.makeText(
                            getApplicationContext(),
                            e.toString(),
                            Toast.LENGTH_SHORT).show();
                }
            }
        });



    }


    static QuickViewExerciseDialog newInstance(String userId) {
        QuickViewExerciseDialog f = new QuickViewExerciseDialog();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString("userId", userId);
        f.setArguments(args);

        return f;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_log, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Diabetes/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // Method for getting data from list of images
    public static List<TimelineRecyclerInfo> getData() {
        List<TimelineRecyclerInfo> data = new ArrayList<>();

        String[] names = {};


        for (int i = 0; i < names.length; i++) {
            TimelineRecyclerInfo current = new TimelineRecyclerInfo();
            current.setTimelineName(names[i]);
            data.add(current);
        }

        return data;
    }


    // the <> contains my custom viewholder; MyViewHolder, and this class will return MyViewHolder as well
    public static class TimelineRecyclerAdapter extends RecyclerView.Adapter<TimelineRecyclerAdapter.MyViewHolder> {

        // emptyList takes care of null pointer exception
        List<TimelineRecyclerInfo> data = Collections.emptyList();
        LayoutInflater inflator;
        Context context;
        FragmentManager fragmentManager;

        public TimelineRecyclerAdapter(Context context, List<TimelineRecyclerInfo> data, FragmentManager fragmentManager) {
            this.context = context;
            inflator = LayoutInflater.from(context);
            this.data = data;
            this.fragmentManager = fragmentManager;
        }

        public void addRow(TimelineRecyclerInfo row){
            data.add(row);
            notifyItemInserted(getItemCount()-1);
        }

        // Called when the recycler view needs to create a new row
        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = inflator.inflate(R.layout.row_session, parent, false);
            MyViewHolder holder = new MyViewHolder(view, new MyViewHolder.MyViewHolderClicks() {
                public void onPotato(View caller, int position) {
                    android.util.Log.d("onPatato", "Poh-tah-tos");


                    //Intent intent = new Intent(context,ViewTimeline.class);
                    //intent.putExtra("selectedId",data.get(position).getParseObjectId());
                    //context.startActivity(intent);

                };
                public void StartWorkout(ImageView callerImage, int position) { android.util.Log.d("StartWorkout","To-m8-tohs"); }
                public void ViewWorkout(ImageView callerImage, int position) {
                    QuickViewExerciseDialog myDialog = newInstance(data.get(position).getParseObjectId());
                    myDialog.show(fragmentManager, "Quick View Workout"); }
                public void EditWorkout(ImageView callerImage) { android.util.Log.d("EditWorkout","To-m8-tohs"); }
            });
            return holder;
        }

        // Setting up the data for each row
        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {

            // This gives us current information list object
            TimelineRecyclerInfo current = data.get(position);

            holder.timelineName.setText(current.getTimelineName());
            holder.date.setText(current.getDate());
            //holder.timerAmount.setText(current.getTimer());
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        // Created my custom view holder
        public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            TextView timelineName;
            ImageView quickViewWorkout;
            TextView date;
            public MyViewHolderClicks mListener;

            // itemView will be my own custom layout View of the row
            public MyViewHolder(View itemView, MyViewHolderClicks listener) {
                super(itemView);

                mListener = listener;
                //Link the objects
                timelineName = (TextView) itemView.findViewById(R.id.timelineName);
                quickViewWorkout = (ImageView) itemView.findViewById(R.id.quickViewWorkout);
                date = (TextView) itemView.findViewById(R.id.date);

                quickViewWorkout.setOnClickListener(this);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                switch(v.getId()) {
                    case R.id.quickViewWorkout:
                        mListener.ViewWorkout((ImageView) v, getAdapterPosition());
                        break;
                    default:
                        mListener.onPotato(v, getAdapterPosition());
                        break;
                }

            }

            public  interface MyViewHolderClicks {
                public void onPotato(View caller, int position);
                public void StartWorkout(ImageView callerImage, int position);
                public void ViewWorkout(ImageView callerImage, int position);
                public void EditWorkout(ImageView callerImage);
            }
        }
    }

    public static class TimelineRecyclerInfo {

        String timelineName;
        Date date;
        ParseObject parseObject;
        //private String timerAmount;

        public TimelineRecyclerInfo() {
            super();

        }

        public TimelineRecyclerInfo(String timelineName, ParseObject parseObject) {
            super();

            if(parseObject != null){
                android.util.Log.d("IsParseObjectNull","Yes");
                this.timelineName = parseObject.get("name").toString();
                this.parseObject = parseObject;
                this.date = parseObject.getCreatedAt();
            }



        }
        public String getTimelineName() {
            return parseObject.get("name").toString();
        }
        public String getDate() {
            // Create an instance of SimpleDateFormat used for formatting
            // the string representation of date (month/day/year)
            DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

            // Get the date today using Calendar object.
            // Using DateFormat format method we can create a string
            // representation of a date with the defined format.
            String reportDate = df.format(this.date);


            return reportDate;
        }
        public void setTimelineName(String timelineName) {
            this.timelineName = parseObject.get("name").toString();
        }

        public String getParseObjectId() {
            return parseObject.getObjectId();
        }
/*
    public String getRep() {
        return repsAmount;
    }
    public void setRep(String repsAmount) {
        this.repsAmount = repsAmount;
    }

    public String getTimer() {
        return timerAmount;
    }
    public void setTimer(String timerAmount) {
        this.timerAmount = timerAmount;
    }

*/
    }


    //==========================================================================================================


}
