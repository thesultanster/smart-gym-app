package com.example.sultan.diabetesmodule.main;

/**
 * Created by Sultan on 7/12/2015.
 */

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.sultan.diabetesmodule.R;

public class EditWorkoutDialog extends DialogFragment implements View.OnClickListener {

    TextView cancel;
    TextView deleteWorkout;
    EditWorkoutCommunicator communicator;
    String workoutId;
    int position;

    static EditWorkoutDialog newInstance(String workoutId, int position) {
        EditWorkoutDialog f = new EditWorkoutDialog();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString("workoutId", workoutId);
        args.putInt("position",position);
        f.setArguments(args);

        return f;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        communicator = (EditWorkoutCommunicator) activity;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_edit_workout, null);
        getDialog().setTitle("Workout Settings");
        cancel = (TextView) view.findViewById(R.id.cancel);
        deleteWorkout = (TextView) view.findViewById(R.id.deleteWorkout);

        cancel.setOnClickListener(this);
        deleteWorkout.setOnClickListener(this);

        workoutId = getArguments().getString("workoutId");
        position = getArguments().getInt("position");

        return view;
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.cancel) {
            dismiss();
        } else if (view.getId() == R.id.yes) {
            dismiss();
        } else if(view.getId() == R.id.deleteWorkout) {
            communicator.DeleteWorkout(workoutId,position);
            dismiss();
        }


    }




}

interface EditWorkoutCommunicator {

    public void DeleteWorkout(String workoutId, int position);
}