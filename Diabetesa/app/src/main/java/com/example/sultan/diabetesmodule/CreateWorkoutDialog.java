package com.example.sultan.diabetesmodule;

/**
 * Created by Sultan on 7/12/2015.
 */

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.sultan.diabetesmodule.main.CreateWorkout;

public class CreateWorkoutDialog extends DialogFragment implements View.OnClickListener {

    TextView cancel;
    TextView yes;
    EditText workoutName;



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_create_workout, null);
        getDialog().setTitle("Create Workout");
        workoutName = (EditText) view.findViewById(R.id.workoutName);
        cancel = (TextView) view.findViewById(R.id.cancel);
        yes = (TextView) view.findViewById(R.id.yes);


        cancel.setOnClickListener(this);
        yes.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.cancel) {
            dismiss();
        } else if (view.getId() == R.id.yes) {
            dismiss();

            Intent activityChangeIntent = new Intent(getActivity(), CreateWorkout.class);
            activityChangeIntent.putExtra("workoutName", workoutName.getText().toString());
            CreateWorkoutDialog.this.startActivity(activityChangeIntent);
        }


    }

}


