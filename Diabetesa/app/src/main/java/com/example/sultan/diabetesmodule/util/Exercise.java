package com.example.sultan.diabetesmodule.util;

import com.parse.ParseObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Sultan on 7/15/2015.
 */
public class Exercise implements Serializable{

    public int[] repArray;
    public ArrayList<Integer> repList;
    public ArrayList<Integer> weightList;
    public int CURRENT_SET;
    public String name;
    public String type;
    public String machineId;

    public Exercise(String name, ArrayList<Integer> repList, ArrayList<Integer> weightList){
        this.repList = repList;
        this.weightList = weightList;
        this.name = name;
        this.type = "MANUAL";
        this.CURRENT_SET = 0;
        this.machineId = "";
    }


    public String getSetsInString(){
        String setString = "";
        int tempSize = repList.size();

        // Create String
        for (int i = 0; i < tempSize; i++) {
            if (i < tempSize - 1)
                setString += repList.get(i) + " ";
            else
                setString += repList.get(i);
        }
        return setString;
    }

    public String getWeightsInString(){
        String weightString = "";
        int tempSize = weightList.size();

        // Create String
        for (int i = 0; i < tempSize; i++) {
            if (i < tempSize - 1)
                weightString += weightList.get(i) + " ";
            else
                weightString += weightList.get(i);
        }
        return weightString;
    }

    public String getName(){
        return name;
    }

    public int[] getRepArray(){
        return repArray;
    }
}
