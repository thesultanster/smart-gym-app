package com.example.sultan.diabetesmodule.util;

import com.parse.ParseObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Sultan on 7/15/2015.
 */
public class Workout implements Serializable{

    // The index of the current exercise
    public int CURRENT_EXERCISE;

    public int NUMBER_OF_EXERCISES;
    public String CURRENT_SET_ID;

    public String userId;
    public String workoutName;
    public ArrayList<Exercise> exerciseList;
    public ArrayList<String> setList;
    public ArrayList<Integer> setIndexes;
    public ArrayList<String> names;
    public ArrayList<Boolean> machineCompletedList;
    public ArrayList<Boolean> isSmartMachine;

    public Workout(String userId) {
        super();
        setDefaultValues(userId);
    }

    public Workout(String workoutName, String userId) {
        this.workoutName = workoutName;
        setDefaultValues(userId);
    }

    private void setDefaultValues(String userId) {
        this.exerciseList = new ArrayList<Exercise>();
        this.CURRENT_EXERCISE = 0;
        this.CURRENT_SET_ID = "";
        this.userId = userId;
        this.setList = new ArrayList<String>();
        this.isSmartMachine = new ArrayList<Boolean>();
        this.machineCompletedList = new ArrayList<Boolean>();
        this.names = new ArrayList<String>();
        this.setIndexes = new ArrayList<Integer>();
    }

    public void addExercises(ArrayList<String> names, ArrayList<String> sets, ArrayList<String> weights) {

        NUMBER_OF_EXERCISES = names.size();

        ArrayList<Integer> repList = new ArrayList<Integer>();
        ArrayList<Integer> weightList = new ArrayList<Integer>();

        // For each exercise, convert set string to replist
        // convert weight string to weightlist
        // Assumption: weight and sets are same size
        for (int i = 0; i < NUMBER_OF_EXERCISES; i++) {
            Scanner setScanner = new Scanner(sets.get(i));
            Scanner weightScanner = new Scanner(weights.get(i));

            repList = new ArrayList<Integer>();
            weightList = new ArrayList<Integer>();
            while (setScanner.hasNextInt()) {
                repList.add(setScanner.nextInt());
                weightList.add(weightScanner.nextInt());
            }
            exerciseList.add(new Exercise(names.get(i), repList, weightList));
            isSmartMachine.add(false);
        }

    }

    public void addExercise(String exerciseName, String set, String weight) {
        NUMBER_OF_EXERCISES++;

        Scanner setScanner = new Scanner(set);
        Scanner weightScanner = new Scanner(weight);

        ArrayList<Integer> repList = new ArrayList<Integer>();
        ArrayList<Integer> weightList = new ArrayList<Integer>();
        while (setScanner.hasNextInt()) {
            repList.add(setScanner.nextInt());
            weightList.add(weightScanner.nextInt());
        }
        exerciseList.add(new Exercise(exerciseName, repList, weightList));
        isSmartMachine.add(false);
    }

    public void completeExercise(int index) {
        //TODO: exerciseList.get(index).setCompleted();
        //TODO: exerciseCompletedList.set(index, true);
    }

    // Push Workout data onto Parse
    public void UploadToDatabase() {

        ParseObject workout = new ParseObject("WorkoutSession");
        workout.put("userId", userId);
        workout.put("Names", Arrays.asList(getNames()));
        workout.put("Sets", Arrays.asList(getSetList()));
        workout.put("Type", Arrays.asList(getIsSmartmachine()));
        workout.put("name", workoutName);
        workout.saveInBackground();
    }

    //Update Workout Data from Parse
    public int getSetIndex() {
        return setIndexes.get(CURRENT_EXERCISE);
    }

    public void incrementSetIndex(){
        setIndexes.set(CURRENT_EXERCISE, setIndexes.get(CURRENT_EXERCISE) + 1);
    }

    public ArrayList<String> getNames() {
        this.names.clear();
        for (int i = 0; i < NUMBER_OF_EXERCISES; i++)
            this.names.add(exerciseList.get(i).getName());
        return this.names;
    }


    // Delete this.setList and just make a temporary object
    public ArrayList<String> getSetList() {
        this.setList.clear();
        for (int i = 0; i < NUMBER_OF_EXERCISES; i++)
            this.setList.add(exerciseList.get(i).getSetsInString());
        return this.setList;
    }

    public ArrayList<String> getWeightList() {
        ArrayList<String> weightList = new ArrayList<String>();
        for (int i = 0; i < NUMBER_OF_EXERCISES; i++)
           weightList.add(exerciseList.get(i).getWeightsInString());
        return weightList;
    }

    public ArrayList<Boolean> getIsSmartmachine() {
        return this.isSmartMachine;
    }

    public Exercise getCurrentExercise(int index) {
        return exerciseList.get(index);
    }

    public Exercise getCurrentExercise() {
        return exerciseList.get(CURRENT_EXERCISE);
    }

    public void RemoveExercise( int index ){

        exerciseList.remove(index);
        isSmartMachine.remove(index);
        NUMBER_OF_EXERCISES--;

    }

}
