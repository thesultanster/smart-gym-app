package com.example.sultan.diabetesmodule;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/**
 * Created by Min on 9/27/2015.
 */
public class OrientationProvider
{
    //Context Variable
    Context context;

    //Sensor Variables
    SensorManager sensorManager;
    Sensor sensor;
    SensorEventListener sensorEventListener;

    //Rotation Matrix
    final float[] geomagnetic = new float[] {1f, 1f, 1f};
    final float[] I = new float[16]; //inclination matrix
    float[] R = new float[16]; //rotation matrix
    final float[] outV = new float[3]; //output vector

    //angles: http://white-smoke.wikifoundry.com/page/Heave,+Pitch,+Roll,+Warp+and+Yaw
    float roll; //x
    float pitch; //y
    float yaw; //z

    //Public Variables
    public boolean isRunning;

    //CONSTRUCTOR [START] --------------------------------------------------------------------------
    public OrientationProvider(Context c)
    {
        //initialization
        context = c;

        //Sensor
        sensorManager = (SensorManager) c.getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ALL);
        sensorEventListener = new SensorEventListener()
        {
            @Override
            public void onSensorChanged(SensorEvent event) {
                SensorManager.getRotationMatrix(R, I, event.values, geomagnetic);
                SensorManager.getOrientation(R, outV); //write orientation to output vector(outV)

                // normalize z on ux, uy
                //float tmp = (float) Math.sqrt(R[8] * R[8] + R[9] * R[9]);
                //tmp = (tmp == 0 ? 0 : R[8] / tmp);

                roll = - (float) Math.toDegrees(outV[2]);
                pitch = (float) Math.toDegrees(outV[1]);
                yaw = (float) Math.toDegrees(outV[0]);
                //yaw = (float) Math.toDegrees(Math.asin(tmp));
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };
        isRunning = false;
    }
    //[END] ----------------------------------------------------------------------------------------

    //UTILITY FUNCTIONS [START] --------------------------------------------------------------------
    public void start()
    {
        isRunning = true;
        sensorManager.registerListener(sensorEventListener, sensor, SensorManager.SENSOR_DELAY_GAME);
    }

    public void end()
    {
        isRunning = false;
        sensorManager.unregisterListener(sensorEventListener, sensor);
    }

    public boolean isRunning(){ return isRunning; }

    public float[] getOrientationVector()
    {
        outV[0] = roll;
        outV[1] = pitch;
        outV[2] = yaw;

        return outV;
    }

    //[END] ----------------------------------------------------------------------------------------
}
