package com.example.sultan.diabetesmodule;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sultan.diabetesmodule.start_workout.StartWorkout;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ViewTimeline extends ActionBarActivity {

    private RecyclerView recyclerView;
    private TimelineMachineRecyclerAdapter adapter;
    Toolbar toolbar;
    String objectId;
    int MAX_PAGES;

    static ArrayList<Boolean> machineCompletedList;
    Button startWorkout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_timeline);

        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            objectId = extras.getString("selectedId");
            android.util.Log.d("OBJECTID", objectId);
        }

        recyclerView = (RecyclerView) findViewById(R.id.timelineRecyclerView);
        adapter = new TimelineMachineRecyclerAdapter(ViewTimeline.this, new ArrayList<TimelineMachineRecyclerInfo>());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(ViewTimeline.this));
        android.util.Log.d("myApp", "oncreateview done");

        startWorkout = (Button) findViewById(R.id.startWorkout);
        startWorkout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click

                Intent intent = new Intent(ViewTimeline.this, StartWorkout.class);
                intent.putExtra("selectedId", objectId);
                intent.putExtra("completedList",machineCompletedList);
                ViewTimeline.this.startActivity(intent);
            }
        });

        Toast.makeText(
                getApplicationContext(),
                objectId,
                Toast.LENGTH_SHORT).show();

        ParseQuery<ParseObject> query = ParseQuery.getQuery("UserWorkout");
        query.getInBackground(objectId, new GetCallback<ParseObject>() {
            public void done(ParseObject workout, ParseException e) {
                if (e == null) {

                    Toast.makeText(
                            getApplicationContext(),
                            "RECIEVED WORKOUT",
                            Toast.LENGTH_SHORT).show();

                    //android.util.Log.d("QUERY", workout.get("machine").toString());

                    ArrayList<String> names = (ArrayList<String>) workout.get("machines");
                    ArrayList<String> sets = (ArrayList<String>) workout.get("sets");

                    MAX_PAGES = names.size();
                    machineCompletedList = new ArrayList<Boolean>(MAX_PAGES);

                    for ( int i = 0; i < names.size(); i++) {
                        machineCompletedList.add(i, false);
                        String name = names.get(i);
                        String set = sets.get(i);
                        adapter.addRow(new TimelineMachineRecyclerInfo(name,set));
                    }


                } else {
                    // something went wrong
                    Toast.makeText(
                            getApplicationContext(),
                            "ERROR WORKOUT",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_timeline, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }





    //=================================== View Workout Recycler Adapter =========================================================

    // the <> contains my custom viewholder; MyViewHolder, and this class will return MyViewHolder as well
    public static class TimelineMachineRecyclerAdapter extends RecyclerView.Adapter<TimelineMachineRecyclerAdapter.MyViewHolder> {

        // emptyList takes care of null pointer exception
        List<TimelineMachineRecyclerInfo> data = Collections.emptyList();
        LayoutInflater inflator;

        public TimelineMachineRecyclerAdapter(Context context, List<TimelineMachineRecyclerInfo> data) {
            inflator = LayoutInflater.from(context);
            this.data = data;
        }

        public void addRow(TimelineMachineRecyclerInfo row){
            data.add(row);
            notifyItemInserted(getItemCount()-1);
        }

        // Called when the recycler view needs to create a new row
        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = inflator.inflate(R.layout.row_timeline_machine, parent, false);
            MyViewHolder holder = new MyViewHolder(view, new MyViewHolder.TimelineMachineInterface() {
                public void onPotato(View caller, int position) {
                    android.util.Log.d("onPatato", "Poh-tah-tos " + position);

                }

                public void onTomato(ImageView callerImage, int position) {
                    android.util.Log.d("onTOmato", "To-m8-tohs");

                }
            });
            return holder;
        }

        // Setting up the data for each row
        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {

            // This gives us current information list object
            TimelineMachineRecyclerInfo current = data.get(position);

            holder.machineName.setText(current.getMachineName());
            holder.sets.setText(current.getSet());
            //holder.timerAmount.setText(current.getTimer());
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        // Created my custom view holder
        public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            TextView machineName;
            TextView sets;
            ImageView excerciseCompleted;
            public TimelineMachineInterface mListener;

            // itemView will be my own custom layout View of the row
            public MyViewHolder(View itemView, TimelineMachineInterface listener) {
                super(itemView);

                mListener = listener;

                //Link the objects
                machineName = (TextView) itemView.findViewById(R.id.machineName);
                sets = (TextView) itemView.findViewById(R.id.sets);
                excerciseCompleted = (ImageView) itemView.findViewById(R.id.excerciseCheckmark);

                //machineName.setOnClickListener(this);
                itemView.setOnClickListener(this);
                excerciseCompleted.setOnClickListener(this);


            }

            @Override
            public void onClick(View v) {
                if (v instanceof ImageView) {
                    if(machineCompletedList.get(getAdapterPosition()) == true) {
                        excerciseCompleted.setImageResource(R.mipmap.unselectedcheckmark);
                        machineCompletedList.set(getAdapterPosition(), false);
                    }
                    else {
                        excerciseCompleted.setImageResource(R.mipmap.selectedcheckmark);
                        machineCompletedList.set(getAdapterPosition(),true);
                    }

                } else {
                    mListener.onPotato(v,getAdapterPosition());
                }
            }

            public interface TimelineMachineInterface {
                public void onPotato(View caller, int position);
                public void onTomato(ImageView callerImage, int position);
            }
        }


    }

    //===========================================================================================================


    //=================================== View Workout Recycler Info Class =====================================
    public static class TimelineMachineRecyclerInfo {


        private String machineName;
        private String sets;
        //private String timerAmount;

        public TimelineMachineRecyclerInfo() {
            super();

        }

        public TimelineMachineRecyclerInfo(String name, String sets) {
            super();
            this.machineName = name;
            this.sets = sets;
        }
        public String getMachineName() {
            return machineName;
        }
        public void setMachineName(String machineName) {
            this.machineName = machineName;
        }

        public String getSet() {
            return sets;
        }
        public void setSet(String sets) {
            this.sets = sets;
        }
/*
    public String getTimer() {
        return timerAmount;
    }
    public void setTimer(String timerAmount) {
        this.timerAmount = timerAmount;
    }

*/
    }


    //===========================================================================================================
}


