package com.example.sultan.diabetesmodule;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sultan.diabetesmodule.util.Exercise;
import com.example.sultan.diabetesmodule.util.Workout;
import com.example.sultan.diabetesmodule.main.SmartGym;
import com.example.sultan.diabetesmodule.util.SerializeObject;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ValueFormatter;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class WorkoutStats extends ActionBarActivity {

    private RecyclerView recyclerView;
    private WorkoutStatsMachineRecyclerAdapter adapter;
    Button saveAndContinue;
    Button saveAs;
    //TODO: Implement Save As


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout_stats);

        Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // getSupportActionBar().setDisplayShowHomeEnabled(true);

        final Workout workout = (Workout) getIntent().getExtras().getSerializable("workout");

        saveAndContinue = (Button) findViewById(R.id.saveAndContinue);


        recyclerView = (RecyclerView) findViewById(R.id.workoutStatRecyclerView);
        adapter = new WorkoutStatsMachineRecyclerAdapter(WorkoutStats.this, new ArrayList<WorkoutStatsMachineRecyclerInfo>());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(WorkoutStats.this));


        android.util.Log.d("Number of exercises", String.valueOf(workout.exerciseList.size()));
        for (int i = 0; i < workout.exerciseList.size(); i++) {
            android.util.Log.d("Exercise name", workout.exerciseList.get(i).getName());

            adapter.addRow(new WorkoutStatsMachineRecyclerInfo(workout.exerciseList.get(i)));

            android.util.Log.d("Exercise Reps", workout.exerciseList.get(i).getSetsInString());

        }



        saveAndContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                for (Exercise exercise : workout.exerciseList) {
                    // SAVE EXERCISE REPS
                    String ser = SerializeObject.objectToString(exercise);
                    if (ser != null && !ser.equalsIgnoreCase("")) {
                        SerializeObject.WriteSettings(getApplicationContext(), ser, exercise.getName() + ".dat");
                    } else {
                        SerializeObject.WriteSettings(getApplicationContext(), "", exercise.getName() + ".dat");
                    }
                }

                ParseQuery<ParseObject> query = ParseQuery.getQuery("UserWorkout");
                query.getInBackground(getIntent().getExtras().get("workoutId").toString(), new GetCallback<ParseObject>() {
                    public void done(ParseObject workoutObject, ParseException e) {
                        if (e == null) {
                            // object will be your game score

                            workoutObject.put("machines", workout.getNames());
                            workoutObject.put("sets", workout.getSetList());
                            workoutObject.put("weights", workout.getWeightList());
                            try {
                                workoutObject.save();
                            } catch (ParseException e1) {
                                e1.printStackTrace();
                            }

                            Intent myIntent = new Intent(WorkoutStats.this, SmartGym.class);
                            //myIntent.putExtra("key", value); //Optional parameters
                            WorkoutStats.this.startActivity(myIntent);


                        } else {
                            // something went wrong
                        }
                    }
                });


                // Save Workout online


            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_workout_stats, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    //=================================== View Workout Recycler Adapter =========================================================

    // the <> contains my custom viewholder; MyViewHolder, and this class will return MyViewHolder as well
    public static class WorkoutStatsMachineRecyclerAdapter extends RecyclerView.Adapter<WorkoutStatsMachineRecyclerAdapter.MyViewHolder> {

        // emptyList takes care of null pointer exception
        List<WorkoutStatsMachineRecyclerInfo> data = Collections.emptyList();
        LayoutInflater inflator;
        Context context;

        public WorkoutStatsMachineRecyclerAdapter(Context context, List<WorkoutStatsMachineRecyclerInfo> data) {
            inflator = LayoutInflater.from(context);
            this.data = data;
            this.context = context;
        }

        public void addRow(WorkoutStatsMachineRecyclerInfo row) {
            data.add(row);
            notifyItemInserted(getItemCount() - 1);
        }

        // Called when the recycler view needs to create a new row
        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = inflator.inflate(R.layout.row_workout_set, parent, false);
            MyViewHolder holder = new MyViewHolder(context, view, new MyViewHolder.TimelineMachineInterface() {
                public void onPotato(View caller, int position) {
                    android.util.Log.d("onPatato", "Poh-tah-tos " + position);

                }

                public void onTomato(ImageView callerImage, int position) {
                    android.util.Log.d("onTOmato", "To-m8-tohs");

                }
            });
            return holder;
        }

        // Setting up the data for each row
        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {

            // This gives us current information list object
            WorkoutStatsMachineRecyclerInfo current = data.get(position);

            holder.machineName.setText(current.getMachineName());
            holder.setupChart(current.exercise);
            holder.smartScore.setText("Smart Score " + String.valueOf(holder.evaluateScore(current.exercise)));
            holder.progressStatus.setText(holder.getProgressText());
            holder.progressStatus.setBackgroundColor(holder.getProgressColor());
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        // Created my custom view holder
        public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            TextView machineName;
            TextView smartScore;
            TextView progressStatus;
            Context context;
            Boolean progress = false;
            int oldScore = -1;
            int newScore;

            public TimelineMachineInterface mListener;

            // itemView will be my own custom layout View of the row
            public MyViewHolder(Context context, View itemView, TimelineMachineInterface listener) {
                super(itemView);
                this.context = context;
                mListener = listener;

                //Link the objects
                machineName = (TextView) itemView.findViewById(R.id.machineName);
                smartScore = (TextView) itemView.findViewById(R.id.smartScore);
                progressStatus = (TextView) itemView.findViewById(R.id.progressStatus);
                itemView.setOnClickListener(this);


            }

            @Override
            public void onClick(View v) {
                if (v instanceof ImageView) {

                } else {
                    mListener.onPotato(v, getAdapterPosition());
                }
            }

            public interface TimelineMachineInterface {
                public void onPotato(View caller, int position);

                public void onTomato(ImageView callerImage, int position);
            }

            private void setupChart(Exercise exercise) {

                ArrayList<Entry> newReps = new ArrayList<Entry>();
                ArrayList<Entry> oldReps = new ArrayList<Entry>();

                // Populate newReps dataset, find largest X value for current set, creat line and add set
                //*********************************************************************
                int largestNumber = exercise.repList.size();
                // Add exercise data
                if (largestNumber != 0)
                    newReps.add(new Entry(exercise.repList.get(0), 0));
                //dataset1.add(new Entry(exercise.repList.get(0), 1));
                for (int i = 0; i < exercise.repList.size(); i++) {
                    newReps.add(new Entry(exercise.repList.get(i), i + 1));
                }

                // Array of Lines
                ArrayList<LineDataSet> lines = new ArrayList<LineDataSet>();
                //Line info
                LineDataSet newSet = new LineDataSet(newReps, "Current Exercise");
                newSet.setLineWidth(1.75f);
                //set.setCircleSize(3f);
                newSet.setColor(Color.parseColor("#e74c3c"));
                //set.setCircleColor(Color.WHITE);
                newSet.setHighLightColor(Color.parseColor("#e74c3c"));
                newSet.setDrawValues(false);
                newSet.setDrawCubic(true);
                //newSet.setDrawFilled(true);
                //newSet.setFillColor(Color.parseColor("#f1c40f"));
                newSet.setDrawCircles(false);
                //newSet.setFillAlpha(2000);


                //*********************************************************************

                // Populate oldreps dataset
                //*********************************************************************************************************************
                ArrayList<Integer> previousRepList;

                // READ REPS
                String serRead = SerializeObject.ReadSettings(context, exercise.getName() + ".dat");
                if (serRead != null && !serRead.equalsIgnoreCase("")) {
                    Object obj = SerializeObject.stringToObject(serRead);
                    // Then cast it to your object and
                    if (obj instanceof Exercise) {
                        // Do something
                        Exercise oldExercise = (Exercise) obj;
                        oldScore = evaluateScore(oldExercise);

                        previousRepList = oldExercise.repList;

                        // If older size is bigger, change to that
                        if (largestNumber < oldExercise.repList.size())
                            largestNumber = oldExercise.repList.size();

                        // Add exercise data
                        if (oldExercise.repList.size() != 0)
                            oldReps.add(new Entry(oldExercise.repList.get(0), 0));
                        //dataset1.add(new Entry(exercise.repList.get(0), 1));
                        for (int i = 0; i < oldExercise.repList.size(); i++) {
                            oldReps.add(new Entry(previousRepList.get(i), i + 1));
                            android.util.Log.d("replist", String.valueOf(previousRepList.get(i)));
                        }


                        //Line info
                        LineDataSet oldSet = new LineDataSet(oldReps, "Last Exercise");
                        oldSet.setDrawCubic(true);
                        oldSet.setDrawValues(false);
                        oldSet.setDrawFilled(true);
                        oldSet.setDrawCircles(false);
                        oldSet.setLineWidth(1.75f);
                        oldSet.setCircleSize(5f);
                        oldSet.setHighLightColor(Color.parseColor("#F4D03F"));
                        oldSet.setColor(Color.parseColor("#F4D03F"));
                        oldSet.setFillColor(Color.parseColor("#F4D03F"));
                        oldSet.setDrawHorizontalHighlightIndicator(false);
                        oldSet.setFillAlpha(3000);

                        lines.add(oldSet);

                    }
                }


                //*********************************************************************************************************************

                // Add line info in array of lines
                lines.add(newSet);


                // Create X axis based on largest x value + 2
                // Add 1 to accomodate the first set taking two points
                String[] xAxis = new String[largestNumber + 1];
                for (int i = 0; i < xAxis.length; i++) {
                    xAxis[i] = String.valueOf(i);
                }


                // Set to chart
                LineChart chart = (LineChart) itemView.findViewById(R.id.chart);
                chart.setData(new LineData(xAxis, lines));
                //chart.setData(new LineData(xAxis,lines));

                // no description text
                chart.setDescription("");
                chart.setNoDataTextDescription("Oh no! There's no data.");

                // enable / disable grid background
                chart.setDrawGridBackground(false);

                // enable touch gestures
                chart.setTouchEnabled(false);

                // enable scaling and dragging
                chart.setDragEnabled(false);
                chart.setScaleEnabled(false);

                // if disabled, scaling can be done on x- and y-axis separately
                chart.setPinchZoom(false);
                //chart.setBackgroundColor(Color.WHITE);

                // get the legend (only possible after setting data)
                Legend l = chart.getLegend();
                l.setEnabled(true);

                chart.getAxisLeft().setEnabled(true);
                chart.getAxisRight().setEnabled(false);
                chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
                chart.getXAxis().setGridColor(Color.WHITE);

                chart.getAxisLeft().setLabelCount(4, true);
                chart.getAxisLeft().setValueFormatter(new ValueFormatter() {
                    @Override
                    public String getFormattedValue(float value) {
                        return String.valueOf((int) (value + 0.5));
                    }
                });
            }


            int evaluateScore(Exercise exercise) {
                double score = 0;

                for (int i = 0; i < exercise.repList.size(); i++) {
                    score += exercise.repList.get(i) * exercise.weightList.get(i);
                }
                score = score / 100;

                newScore = ((int) (score + 0.5));
                return newScore;
            }

            public String getProgressText() {

                if (newScore > oldScore)
                    return "Progressed!";
                else if (newScore == oldScore)
                    return "Same As Before";
                else
                    return "Decline!";

            }

            public int getProgressColor() {
                if (newScore > oldScore)
                    return Color.parseColor("#2ECC71");
                else if (newScore == oldScore)
                    return Color.parseColor("#3498db");
                else
                    return Color.parseColor("#e74c3c");

            }
        }


    }

    //===========================================================================================================


    //=================================== View Workout Recycler Info Class =====================================
    public static class WorkoutStatsMachineRecyclerInfo {


        String machineName;
        Exercise exercise;

        public WorkoutStatsMachineRecyclerInfo() {
            super();

        }

        public WorkoutStatsMachineRecyclerInfo(Exercise exercise) {
            super();
            this.machineName = exercise.getName();
            this.exercise = exercise;
        }

        public String getMachineName() {
            return machineName;
        }

        public void setMachineName(String machineName) {
            this.machineName = machineName;
        }
    }


    //===========================================================================================================

}
