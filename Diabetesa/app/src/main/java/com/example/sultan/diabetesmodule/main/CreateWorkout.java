package com.example.sultan.diabetesmodule.main;

import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.example.sultan.diabetesmodule.R;
import com.example.sultan.diabetesmodule.util.Workout;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class CreateWorkout extends ActionBarActivity implements AddMachineCommunicator {


    TextView addMachine;
    TextView saveWorkout;
    TextView workoutName;

    String weight;

    private RecyclerView recyclerView;
    private CreateWorkoutRecyclerAdapter adapter;
    private List<ParseObject> machines;
    private ArrayList<String> weightList;

    Workout workout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_workout);

        recyclerView = (RecyclerView) findViewById(R.id.createWorkoutMachineRecyclerView);
        adapter = new CreateWorkoutRecyclerAdapter(CreateWorkout.this, new ArrayList<CreateWorkoutRecyclerInfo>());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(CreateWorkout.this));

        // Get Workout name from bundle
        String tempName = getIntent().getExtras().getString("workoutName");

        // Create Workout object from initial values
        workout = new Workout(tempName);


        // Show Workout Name
        workoutName = (TextView) findViewById(R.id.workoutName);
        workoutName.setText(tempName);

        weightList = new ArrayList<String>();

        // When Clicked, show Add Machine Dialog
        addMachine = (TextView) findViewById(R.id.addMachine);
        addMachine.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                FragmentManager fragmentManager = getFragmentManager();
                AddExerciseDialog myDialog = new AddExerciseDialog();
                myDialog.show(fragmentManager, "My Dialog");
            }
        });

        // Save Workout to Database
        saveWorkout = (TextView) findViewById(R.id.saveWorkout);
        saveWorkout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final ProgressDialog progress;
                progress = ProgressDialog.show(CreateWorkout.this, "Saving Workout", "Sit Tight", true);

                ParseObject userWorkout = new ParseObject("UserWorkout");
                userWorkout.put("name", workoutName.getText().toString());
                userWorkout.put("owner", ParseUser.getCurrentUser());
                userWorkout.put("userId", ParseUser.getCurrentUser().getObjectId());
                userWorkout.addAll("machines", Arrays.asList(adapter.getMachineNames()));
                userWorkout.addAll("sets", Arrays.asList(adapter.getSets()));
                userWorkout.addAll("weights",weightList );
                userWorkout.saveInBackground(new SaveCallback() {
                    public void done(ParseException e) {
                        if (e == null) {

                            Intent activityChangeIntent = new Intent(CreateWorkout.this, SmartGym.class);
                            CreateWorkout.this.startActivity(activityChangeIntent);

                        } else {

                        }
                        progress.dismiss();
                    }
                });
            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_workout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onDialogMessage(String exerciseName, List<NumberPicker> numberPickerList, String weight) {


        String setString = "";
        String weightString = "";
        int tempSize = numberPickerList.size();


        // Create String
        for (int i = 0; i < tempSize; i++) {

            if (i < tempSize - 1) {
                setString += numberPickerList.get(i).getValue() + " ";
                weightString += weight + " ";
            }
            else {
                setString += numberPickerList.get(i).getValue();
                weightString += weight;
            }
        }

        adapter.addRow(new CreateWorkoutRecyclerInfo(exerciseName, setString));
        weightList.add(weightString);

    }



    //=================================== Create Workout Recycler Adapter Class ====================================

    // the <> contains my custom viewholder; MyViewHolder, and this class will return MyViewHolder as well
    public static class CreateWorkoutRecyclerAdapter extends RecyclerView.Adapter<CreateWorkoutRecyclerAdapter.MyViewHolder> {

        // emptyList takes care of null pointer exception
        List<CreateWorkoutRecyclerInfo> data = Collections.emptyList();
        LayoutInflater inflator;
        Context context;

        public CreateWorkoutRecyclerAdapter(Context context, List<CreateWorkoutRecyclerInfo> data) {
            this.context = context;
            inflator = LayoutInflater.from(context);
            this.data = data;
        }

        public void addRow(CreateWorkoutRecyclerInfo row) {
            data.add(row);
            notifyItemInserted(getItemCount() - 1);
        }

        public void deleteRow(int position) {
            data.remove(position);
            notifyItemRemoved(position);
        }

        public String[] getMachineNames() {
            List<CreateWorkoutRecyclerInfo> names = this.data;
            String[] array = new String[names.size()];
            for(int i = 0; i < names.size(); i++) array[i] = names.get(i).getMachineName();
            return array;
        }

        public String[] getSets() {
            List<CreateWorkoutRecyclerInfo> sets = this.data;
            String[] array = new String[sets.size()];
            for(int i = 0; i < sets.size(); i++) array[i] = sets.get(i).getSet();
            return array;
        }

        // Called when the recycler view needs to create a new row
        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = inflator.inflate(R.layout.row_create_workout, parent, false);
            MyViewHolder holder = new MyViewHolder(view, new MyViewHolder.MyViewHolderClicks() {
                public void onPotato(View caller) {
                    android.util.Log.d("onPatato", "Poh-tah-tos");
                    //context.startActivity(new Intent(context, ViewTimeline.class));
                }

                public void onDeleteRow(int position) {
                    deleteRow(position);
                }
            });
            return holder;
        }

        // Setting up the data for each row
        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {

            // This gives us current information list object
            CreateWorkoutRecyclerInfo current = data.get(position);

            holder.machineName.setText(current.getMachineName());
            holder.sets.setText(current.getSet());
            //android.util.Log.d("onBindView", current.getSet());
            //holder.timerAmount.setText(current.getTimer());
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        // Created my custom view holder
        public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            TextView machineName;
            TextView sets;
            ImageView deleteMachine;

            public MyViewHolderClicks mListener;

            // itemView will be my own custom layout View of the row
            public MyViewHolder(View itemView, MyViewHolderClicks listener) {
                super(itemView);

                mListener = listener;

                //Link the objects
                machineName = (TextView) itemView.findViewById(R.id.machineName);
                sets = (TextView) itemView.findViewById(R.id.sets);
                deleteMachine = (ImageView) itemView.findViewById(R.id.deleteMachine);

                deleteMachine.setOnClickListener(this);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                if (v instanceof ImageView) {
                    mListener.onDeleteRow(getAdapterPosition());
                } else {
                    mListener.onPotato(v);
                }

            }

            public interface MyViewHolderClicks {
                public void onPotato(View caller);
                public void onDeleteRow(int position);
            }
        }
    }

    //===========================================================================================================


    //=================================== Create Workout Recycler Info Class ====================================

    public static class CreateWorkoutRecyclerInfo {


        private String machineName;
        private String set;
        ParseObject parseObject;
        //private String timerAmount;

        public CreateWorkoutRecyclerInfo() {
            super();

        }

        public CreateWorkoutRecyclerInfo(String machineName, String set) {
            super();
            //this.timelineName = parseObject.get("name").toString();
            //this.parseObject = parseObject;
            this.machineName = machineName;
            this.set = set;
        }

        public String getMachineName() {
            //return parseObject.get("name").toString();
            return this.machineName;
        }

        public void setMachineName(String machineName) {
            //this.timelineName = parseObject.get("name").toString();
            this.machineName = machineName;
        }

        public String getSet() {
            return this.set;
        }
        public void setSet(String set) {
            this.set = set;
        }


    }

    //===========================================================================================================


}
