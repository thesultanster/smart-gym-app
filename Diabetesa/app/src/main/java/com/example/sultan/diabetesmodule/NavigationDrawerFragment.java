package com.example.sultan.diabetesmodule;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.sultan.diabetesmodule.demo_d.DraggableExampleActivity;
import com.example.sultan.diabetesmodule.log.Log;
import com.example.sultan.diabetesmodule.main.SmartGym;
import com.parse.LogOutCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

/**
 * A simple {@link Fragment} subclass.
 */
public class NavigationDrawerFragment extends Fragment {

    // constant represents name of the file
    public static final String PREF_FILE_NAME="testpref";
    public static final String KEY_USER_LEARNED_DRAWER="user_learned_drawer";

    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private View containerView;

    ImageButton home;
    ImageButton log;
    ImageButton stats;
    ImageButton smartGym;
    ImageButton Diabetes;

    TextView fat;
    TextView height;
    TextView weight;
    TextView name;

    Button settings;

    ParseUser user;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mUserLearnedDrawer=Boolean.valueOf(readFromPreferences(getActivity(),KEY_USER_LEARNED_DRAWER,"false"));

        // coming back from rotation
        if(savedInstanceState != null) {
            mFromSavedInstanceState = true;
        }

    }

    //indicates whether the user is aware of the drawers existence
    private boolean mUserLearnedDrawer;
    //indicates whether fragment is started first time
    private boolean mFromSavedInstanceState;

    public NavigationDrawerFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        home = (ImageButton) v.findViewById(R.id.homeIcon);
        log = (ImageButton) v.findViewById(R.id.logIcon);
        stats = (ImageButton) v.findViewById(R.id.statsIcon);
        smartGym = (ImageButton) v.findViewById(R.id.smartGymIcon);
        Diabetes = (ImageButton) v.findViewById(R.id.diabetesIcon);

        settings = (Button) v.findViewById(R.id.settingsButton);

        fat = (TextView) v.findViewById(R.id.fat);
        weight = (TextView) v.findViewById(R.id.weight);
        height = (TextView) v.findViewById(R.id.height);
        name = (TextView) v.findViewById(R.id.name);





        home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                Intent intent = new Intent(getActivity(), SmartGym.class);
                startActivity(intent);
            }
        });

        log.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                Intent intent = new Intent (getActivity(), Log.class);
                startActivity(intent);
            }
        });

        stats.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                Intent intent = new Intent(getActivity(), Stats.class);
                startActivity(intent);
            }
        });

        smartGym.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                Intent intent = new Intent (getActivity(), SmartGym.class);
                startActivity(intent);
            }
        });

        Diabetes.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                Intent intent = new Intent (getActivity(), DraggableExampleActivity.class);
                startActivity(intent);
            }
        });

        settings.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                ParseUser.logOutInBackground(new LogOutCallback() {
                    @Override
                    public void done(ParseException e) {

                        Intent intent = new Intent (getActivity(), Login.class);
                        startActivity(intent);
                        getActivity().finish();
                    }
                });
            }
        });

        smartGym.setVisibility(View.GONE);
        Diabetes.setVisibility(View.GONE);



        return v;
    }


    public void setUp( int fragmentId, DrawerLayout drawerLayout, Toolbar toolbar)
    {
        containerView=getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(),drawerLayout,toolbar,R.string.drawer_open, R.string.drawer_close){
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                user = ParseUser.getCurrentUser();
                //android.util.Log.d("NavDraw User", ParseUser.getCurrentUser().getUsername());
                name.setText(user.getString("firstName") + " " + user.getString("lastName"));


                if(user != null) {

                    String temp = String.valueOf(user.get("fat")) + "<font color='#C3D8DD'> %</font>";
                    fat.setText(Html.fromHtml(temp));
                    temp = String.valueOf(user.get("feet")) + "<font color='#C3D8DD'>'</font>" + String.valueOf(user.get("inches")) + "<font color='#C3D8DD'>\"</font>";
                    height.setText(Html.fromHtml(temp));
                    temp = String.valueOf(String.valueOf(user.get("weight"))) + "<font color='#C3D8DD'> lbs</font>";
                    weight.setText(Html.fromHtml(temp));
                }



                // IF user have never seen drawer before
                if(!mUserLearnedDrawer)
                {
                    // yes it is open
                    mUserLearnedDrawer=true;
                    savedToPreferences(getActivity(),KEY_USER_LEARNED_DRAWER,mUserLearnedDrawer+"");
                }
                // redraw make activity draw actionbar again
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }
        };

        // IF the user has never seen drawer, and this is very first time frag is starting
        if(!mUserLearnedDrawer && !mFromSavedInstanceState)
        {
            // open the fragment in activity main
            mDrawerLayout.openDrawer(containerView);
        }

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

    }

    //Store drawer value in shared preference file. If it is saved then we will not open drawer
    public static void savedToPreferences(Context context, String preferenceName, String preferenceValue){
        // mode private means our app is the only one to modify sharedPreference value
        SharedPreferences sharedPreferences=context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(preferenceName,preferenceValue);
        editor.apply();
    }

    public static String readFromPreferences(Context context, String preferenceName, String defaultValue){
        // mode private means our app is the only one to modify sharedPreference value
        SharedPreferences sharedPreferences=context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(preferenceName,defaultValue);
    }

}