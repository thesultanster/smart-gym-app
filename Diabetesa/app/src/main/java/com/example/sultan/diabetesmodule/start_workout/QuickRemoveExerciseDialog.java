package com.example.sultan.diabetesmodule.start_workout;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.sultan.diabetesmodule.R;

public class QuickRemoveExerciseDialog extends DialogFragment implements View.OnClickListener {

    TextView cancel;
    TextView yes;
    QuickRemoveExerciseCommunicator communicator;




    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        communicator = (QuickRemoveExerciseCommunicator) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_quick_remove_exercise, null);
        getDialog().setTitle("Remove Current Exercise?");
        cancel = (TextView) view.findViewById(R.id.cancel);
        yes = (TextView) view.findViewById(R.id.yes);

        cancel.setOnClickListener(this);
        yes.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.cancel) {
            dismiss();
        } else if (view.getId() == R.id.yes) {
            dismiss();

            communicator.onDialogRemoveCurrentExercise();
        }


    }

}


interface QuickRemoveExerciseCommunicator {

    public void onDialogRemoveCurrentExercise();
}
