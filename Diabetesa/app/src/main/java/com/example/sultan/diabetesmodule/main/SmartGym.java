package com.example.sultan.diabetesmodule.main;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sultan.diabetesmodule.CreateWorkoutDialog;
import com.example.sultan.diabetesmodule.Login;
import com.example.sultan.diabetesmodule.NavigationDrawerFragment;
import com.example.sultan.diabetesmodule.R;
import com.example.sultan.diabetesmodule.SearchSmartGym;
import com.example.sultan.diabetesmodule.start_workout.StartWorkout;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* This is the Home activity where currently the user can Create a new workout or start their saved
 * workouts
 * This activity contains:
 * Recycler View
 * Dialogs
 * Parse Database Queries
 */

public class SmartGym extends ActionBarActivity implements EditWorkoutCommunicator {

    private android.support.v7.widget.Toolbar toolbar;
    TextView createWorkout;
    TextView findEditGym;

    private RecyclerView recyclerView;
    private TimelineRecyclerAdapter adapter;
    List<ParseObject> timelines;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smart_gym);

        // If their is no Parse User, then go to Login page
        Intent intent;
        if(ParseUser.getCurrentUser() == null){
            intent = new Intent(this, Login.class);
            startActivity(intent);
        }

        // Action Bar Setup
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // Navigation Drawer Setup
        NavigationDrawerFragment drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);


        // Setup Buttons and their clicking functionalities
        SetupButtons();

        // RecyclerView Setup
        // This will show user workouts
        // Handle a click for the whole row and individual buttons
        // Open a QuickView dialog is they wanna see their workouts quickly
        FragmentManager fragmentManager = getFragmentManager();
        recyclerView = (RecyclerView) findViewById(R.id.yourTimelineRecyclerView);
        adapter = new TimelineRecyclerAdapter(SmartGym.this, new ArrayList<TimelineRecyclerInfo>(), fragmentManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(SmartGym.this));

        ParseQuery<ParseObject> query = ParseQuery.getQuery("UserWorkout");
        query.whereEqualTo("owner", ParseUser.getCurrentUser());
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> timelineNames, ParseException e) {
                if (e == null) {
                    Toast.makeText(
                            getApplicationContext(),
                            String.valueOf(timelineNames.size()),
                            Toast.LENGTH_SHORT).show();

                    ParseObject.pinAllInBackground(timelineNames);


                    timelines = timelineNames;
                    for (ParseObject name : timelineNames) {
                        adapter.addRow(new TimelineRecyclerInfo(name.getString("name"), name));
                    }

                } else {
                    Toast.makeText(
                            getApplicationContext(),
                            e.toString(),
                            Toast.LENGTH_SHORT).show();
                }
            }
        });



    }

    private void SetupButtons(){

        // Setup Create Workout Button
        createWorkout = (TextView) findViewById(R.id.createWorkout);
        createWorkout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                // This will open up a Dialog to prompt user of new workout name
                FragmentManager fragmentManager = getFragmentManager();
                CreateWorkoutDialog myDialog = new CreateWorkoutDialog();
                myDialog.show(fragmentManager, "Create Workout");

            }
        });

        findEditGym = (TextView) findViewById(R.id.findEditGym);
        findEditGym.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                // Change Activity to SerchSmartGym
                Intent intent = new Intent(SmartGym.this, SearchSmartGym.class);
                startActivity(intent);

            }
        });


    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_smart_gym, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Diabetes/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void DeleteWorkout(String workoutId, int position) {

        // Delete From Database
        ParseQuery<ParseObject> query = ParseQuery.getQuery("UserWorkout");
        query.getInBackground(workoutId, new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (e == null) {

                    object.deleteInBackground();

                } else {
                    // something went wrong
                }
            }
        });

        // Delete From List
        adapter.deleteRow(position);
    }


    // the <> contains my custom viewholder; MyViewHolder, and this class will return MyViewHolder as well
    public static class TimelineRecyclerAdapter extends RecyclerView.Adapter<TimelineRecyclerAdapter.MyViewHolder> {

        // emptyList takes care of null pointer exception
        List<TimelineRecyclerInfo> data = Collections.emptyList();
        LayoutInflater inflator;
        Context context;
        FragmentManager fragmentManager;

        public TimelineRecyclerAdapter(Context context, List<TimelineRecyclerInfo> data, FragmentManager fragmentManager) {
            this.context = context;
            inflator = LayoutInflater.from(context);
            this.data = data;
            this.fragmentManager = fragmentManager;
        }

        public void addRow(TimelineRecyclerInfo row){
            data.add(row);
            notifyItemInserted(getItemCount()-1);
        }

        public void deleteRow(int position) {
            data.remove(position);
            notifyItemRemoved(position);
        }

        // Called when the recycler view needs to create a new row
        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            final View view = inflator.inflate(R.layout.row_workout, parent, false);
            MyViewHolder holder = new MyViewHolder(view, new MyViewHolder.MyViewHolderClicks() {
                public void RowClickStartWorkout(View caller, int position) {
                    QuickViewExerciseDialog myDialog = QuickViewExerciseDialog.newInstance(data.get(position).getParseObjectId());
                    myDialog.show(fragmentManager, "Quick View Workout");
                };
                public void StartWorkout(ImageView callerImage, int position) {
                    Intent intent = new Intent(context, StartWorkout.class);
                    intent.putExtra("selectedId", data.get(position).getParseObjectId());
                    view.getContext().startActivity(intent);
                }
                public void ViewWorkout(ImageView callerImage, int position) {
                    QuickViewExerciseDialog myDialog = QuickViewExerciseDialog.newInstance(data.get(position).getParseObjectId());
                    myDialog.show(fragmentManager, "Quick View Workout");
                }

                public void EditWorkout(ImageView callerImage, int position) {
                    EditWorkoutDialog myDialog = EditWorkoutDialog.newInstance(data.get(position).getParseObjectId(), position);
                    myDialog.show(fragmentManager, "Workout Settings");
                }


            });

            return holder;
        }

        // Setting up the data for each row
        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {

            // This gives us current information list object
            TimelineRecyclerInfo current = data.get(position);

            holder.timelineName.setText(current.getTimelineName());
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        // Created my custom view holder
        public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            TextView timelineName;
            ImageView startWorkoutNow;
            ImageView editWorkout;
            ImageView quickViewWorkout;
            public MyViewHolderClicks mListener;

            // itemView will be my own custom layout View of the row
            public MyViewHolder(View itemView, MyViewHolderClicks listener) {
                super(itemView);

                mListener = listener;
                //Link the objects
                timelineName = (TextView) itemView.findViewById(R.id.timelineName);
                startWorkoutNow = (ImageView) itemView.findViewById(R.id.startWorkoutNow);
                editWorkout = (ImageView) itemView.findViewById(R.id.editWorkout);
                quickViewWorkout = (ImageView) itemView.findViewById(R.id.quickViewWorkout);

                startWorkoutNow.setOnClickListener(this);
                editWorkout.setOnClickListener(this);
                quickViewWorkout.setOnClickListener(this);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                switch(v.getId()) {
                    case R.id.startWorkoutNow:
                        mListener.StartWorkout((ImageView) v, getAdapterPosition());
                        break;
                    case R.id.quickViewWorkout:
                        mListener.ViewWorkout((ImageView) v, getAdapterPosition());
                        break;
                    case R.id.editWorkout:
                        mListener.EditWorkout((ImageView) v, getAdapterPosition());
                        break;
                    default:
                        mListener.RowClickStartWorkout(v, getAdapterPosition());
                        break;
                }
            }

            public  interface MyViewHolderClicks {
                public void RowClickStartWorkout(View caller, int position);
                public void StartWorkout(ImageView callerImage, int position);
                public void ViewWorkout(ImageView callerImage, int position);
                public void EditWorkout(ImageView callerImage, int position);
            }
        }
    }

    public static class TimelineRecyclerInfo {

        String timelineName;
        ParseObject parseObject;
        //private String timerAmount;

        public TimelineRecyclerInfo() {
            super();

        }

        public TimelineRecyclerInfo(String timelineName, ParseObject parseObject) {
            super();
            this.timelineName = parseObject.get("name").toString();
            this.parseObject = parseObject;
        }
        public String getTimelineName() {
            return parseObject.get("name").toString();
        }
        public void setTimelineName(String timelineName) {
            this.timelineName = parseObject.get("name").toString();
        }

        public String getParseObjectId() {
            return parseObject.getObjectId();
        }

    }


    //==========================================================================================================


}
