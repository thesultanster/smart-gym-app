package com.example.sultan.diabetesmodule.main;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.sultan.diabetesmodule.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sultan on 7/12/2015.
 */
public class AddExerciseDialog extends DialogFragment implements View.OnClickListener{

    TextView cancel;
    TextView yes;
    Button addSet;
    EditText machineName;
    EditText weight;
    AddMachineCommunicator communicator;
    NumberPicker numberPicker1;
    NumberPicker numberPicker2;
    NumberPicker numberPicker3;
    ImageView deleteSet;
    LinearLayout ll;
    HorizontalScrollView hsv;
    List<NumberPicker> setList;
    int sets;

    String[] spinnerValues = {"Squat Machine", "Leg Press Machine", "Calves Machine", "Lateral Pull Down", "Push Up","Pull Ups","Bench Press"};
    String[] spinnerSubs = {"Legs   Smart Machine", "Legs   Smart Machine", "Legs   Smart Machine", "Back   Smart Machine", "Triceps   Freestyle","Biceps   Freestyle","Triceps   Freestyle"};
    int total_images[] = {
            R.mipmap.dumbell,
            R.mipmap.dumbell,
            R.mipmap.dumbell,
            R.mipmap.dumbell,
            R.mipmap.dumbell,
            R.mipmap.dumbell,
            R.mipmap.dumbell};



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        communicator = (AddMachineCommunicator) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.dialog_add_machine, null);
        setList = new ArrayList<NumberPicker>();
        ll = (LinearLayout) view.findViewById(R.id.setLinearLayout);
        hsv = (HorizontalScrollView) view.findViewById(R.id.setScrollView);
        cancel = (TextView) view.findViewById(R.id.cancel);
        yes = (TextView) view.findViewById(R.id.yes);
        addSet = (Button) view.findViewById(R.id.addSet);
        deleteSet = (ImageView) view.findViewById(R.id.deleteSet);
        machineName = (EditText) view.findViewById(R.id.machineName);
        weight = (EditText) view.findViewById(R.id.weightValue);
        numberPicker1 = (NumberPicker) view.findViewById(R.id.setNumber1);
        numberPicker1.setMaxValue(100);    //set maximum val
        numberPicker1.setMinValue(1);     //set minimum val
        numberPicker1.setValue(10);    //set Initial val

        numberPicker2 = (NumberPicker) view.findViewById(R.id.setNumber2);
        numberPicker2.setMaxValue(100);    //set maximum val
        numberPicker2.setMinValue(1);     //set minimum val
        numberPicker2.setValue(8);    //set Initial val

        numberPicker3 = (NumberPicker) view.findViewById(R.id.setNumber3);
        numberPicker3.setMaxValue(100);    //set maximum val
        numberPicker3.setMinValue(1);     //set minimum val
        numberPicker3.setValue(7);    //set Initial val

        setList.add(numberPicker1);
        setList.add(numberPicker2);
        setList.add(numberPicker3);
        sets = 3;

        cancel.setOnClickListener(this);
        yes.setOnClickListener(this);
        addSet.setOnClickListener(this);
        deleteSet.setOnClickListener(this);

        Spinner mySpinner = (Spinner) view.findViewById(R.id.spinner_show);
        mySpinner.setAdapter(new MyAdapter(getActivity(), R.layout.row_spinner, spinnerValues));
        mySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                TextView name = (TextView) selectedItemView.findViewById(R.id.text_main_seen);
                machineName.setText(name.getText());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        return view;
    }

    @Override
    public void onClick(View view){

        if(view.getId() == R.id.cancel){
            dismiss();
        }
        else if (view.getId() == R.id.yes){
            dismiss();
            communicator.onDialogMessage(machineName.getText().toString(), setList, weight.getText().toString());
        }
        else if (view.getId() == R.id.deleteSet){
            int count = ll.getChildCount();
            if(count > 2) {
                ll.removeViewAt(count-2);
                sets--;
                setList.remove(sets);
            }
        }
        else if(view.getId() == R.id.addSet){
            NumberPicker newPicker = new NumberPicker(view.getContext());
            //newPicker.getLayoutParams().height = Number.LayoutParams.WRAP_CONTENT;
            newPicker.setMaxValue(100);    //set maximum val
            newPicker.setMinValue(1);     //set minimum val
            newPicker.setValue(10);    //set Initial val
            ll.addView(newPicker, sets);
            sets++;

            setList.add(newPicker);

            //hsv.fullScroll(View.FOCUS_DOWN);

            hsv.post(new Runnable() {
                @Override
                public void run() {
                    hsv.fullScroll(View.FOCUS_RIGHT);
                }
            });

        }

    }

    public class MyAdapter extends ArrayAdapter<String> {
        public MyAdapter(Context ctx, int txtViewResourceId, String[] objects) {
            super(ctx, txtViewResourceId, objects);
        }

        @Override
        public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
            return getCustomView(position, cnvtView, prnt);
        }

        @Override
        public View getView(int pos, View cnvtView, ViewGroup prnt) {
            return getCustomView(pos, cnvtView, prnt);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View mySpinner = inflater.inflate(R.layout.row_spinner, parent, false);
            TextView main_text = (TextView) mySpinner.findViewById(R.id.text_main_seen);
            main_text.setText(spinnerValues[position]);
            TextView subSpinner = (TextView) mySpinner.findViewById(R.id.sub_text_seen);
            subSpinner.setText(spinnerSubs[position]);
            ImageView left_icon = (ImageView) mySpinner.findViewById(R.id.left_pic);
            left_icon.setImageResource(total_images[position]);
            return mySpinner;
        }
    }

}

interface AddMachineCommunicator{

    public void onDialogMessage(String message, List<NumberPicker> setList, String weight);
}