package com.example.sultan.diabetesmodule.start_workout;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.ParcelUuid;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.*;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sultan.diabetesmodule.util.BenchRepper;
import com.example.sultan.diabetesmodule.util.MyBroadCastReciever;
import com.example.sultan.diabetesmodule.R;
import com.example.sultan.diabetesmodule.util.SlidingTabLayout;
import com.example.sultan.diabetesmodule.util.Workout;
import com.example.sultan.diabetesmodule.WorkoutStats;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class StartWorkout extends AppCompatActivity implements QuickAddExerciseCommunicator, QuickRemoveExerciseCommunicator {
    public static final String PREFERENCE_NAME = "";

    ViewPager machineViewpager;
    SlidingTabLayout machineTabs;
    android.support.v7.widget.Toolbar toolbar;
    ImageView machineCompleted;
    ImageView addSet;
    ImageView deleteSet;
    ArrayList<String> names;
    ArrayList<String> sets;

    ImageView rightSkip;
    ImageView leftSkip;

    int MAX_PAGES;
    long MACHINE_SESSION_START;
    long MACHINE_SESSION_END;
    int completedCount = 0;
    ArrayList<Boolean> machineCompletedList;
    String objectId;
    TextView addText;
    TextView deleteText;
    TextView completeText;
    TextView exerciseSets;
    TextView currentRepBar;
    TextView machineName;
    TextView noSmart;
    TextView connectionStatus;
    Button connectToMachine;
    Button updateRepButton;
    Button disconnectFromMachine;
    Button incrementRep;
    Button incrementSet;
    Map<String, String> machineIds = new HashMap<String, String>();
    MachineProgressPagerAdapter machineProgressPagerAdapter;

    Workout workout;
    LinearLayout setsLinearLayout;
    LinearLayout weightsLinearLayout;
    LinearLayout smartExerciseConsole;
    LinearLayout customExerciseConsole;
    LinearLayout setNumberLinearLayout;
    View machineStatusColor;

    ParseObject setObject;
    int dareps = 0;
    int dasets = 0;

    // IP Variables
    Client client;
    int PORT = 12345;//SET A CONSTANT VARIABLE PORT
    String CurrentHost = "Apples";//SET A CONSTANT VARIABLE HOST
    String RecievedMachineId = "Bananas";


    // Bench Press
    boolean incAllowed = true;
    int repCount = 0;
    int repTime = 0;
    BenchRepper benchRepper;


    // NFC Variables
    public static final String MIME_TEXT_PLAIN = "text/plain";
    public static final String TAG = "NfcDemo";
    private NfcAdapter mNfcAdapter;


    private OutputStream outputStream;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_workout);

        MACHINE_SESSION_START = System.currentTimeMillis();
        setObject = new ParseObject("sets");

        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        machineIds.put("Chest Fly", "Gr9FOMRoeK");
        //machineIds.put("Leg Press Machine", "45jQtCrKgZ");
        //machineIds.put("Calves Machine", "WJ5KNDj2Eg");
        //machineIds.put("Lateral Pull Down", "XL2z16Fvkq");
        //machineIds.put("Row Machine", "1vdidzNIGL");
        //machineIds.put("Bench Press", "PJ8IdGHYGS");


        inflateVariables();


        customExerciseConsole.setVisibility(View.GONE);
        machineStatusColor = (View) findViewById(R.id.statusColor);

        disconnectFromMachine.setVisibility(View.GONE);
        setsLinearLayout.setGravity(Gravity.CENTER);
        weightsLinearLayout.setGravity(Gravity.CENTER);

        workout = new Workout(ParseUser.getCurrentUser().getObjectId());

        // Right Skip

        rightSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SwipeExercise("RIGHT", 1);
            }
        });


        // Left Skip
        leftSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SwipeExercise("LEFT", -1);
            }
        });


//=============== NFC
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);

        if (mNfcAdapter == null) {
            // Stop here, we definitely need NFC
            Toast.makeText(this, "This device doesn't support NFC.", Toast.LENGTH_LONG).show();
            finish();
            return;

        }

        if (!mNfcAdapter.isEnabled()) {
            android.util.Log.d("NFC", "NFC DISABLED");
        } else {
            android.util.Log.d("NFC", "NFC ENABLED");
        }

        handleIntent(getIntent());
//====================

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            objectId = extras.getString("selectedId");
            //machineCompletedList = (ArrayList<Boolean>) getIntent().getSerializableExtra("completedList");
            machineCompletedList = new ArrayList<Boolean>();
            machineCompletedList.add(false);
            //MAX_PAGES = machineCompletedList.size();


            if (machineCompletedList.get(workout.CURRENT_EXERCISE) == false) {
                machineCompleted.setImageResource(R.mipmap.unselectedcheckmark);
                //machineState.setText("Incomplete");

            } else {
                machineCompleted.setImageResource(R.mipmap.selectedcheckmark);
                //machineState.setText("Complete");
            }

            if (objectId != null) {
                android.util.Log.d("OBJECTID", objectId);
                SetupWorkoutFromObject(objectId);

            } else {
                android.util.Log.d("NFC", "NO WORKOUT MACHINE THROUGH NFC DETECTED");
                addSet.setVisibility(View.INVISIBLE);
                deleteSet.setVisibility(View.INVISIBLE);
                machineCompleted.setVisibility(View.INVISIBLE);
                rightSkip.setVisibility(View.INVISIBLE);
                leftSkip.setVisibility(View.INVISIBLE);
                noSmart.setText("Add Exercise");
                SetupWorkoutFromScratch();
            }

        }


        // Machine Completed Button toggle
        machineCompleted.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                if (machineCompletedList.get(workout.CURRENT_EXERCISE) == true) {
                    ToggleCompletedUp();

                } else {

                    // if it is a smart machine enabled exercise
                    // and if it is currently connected to a smart machine
                    if (machineIds.get(workout.getCurrentExercise().getName()) != null && connectionStatus.getText().toString().equals("Connected")) {
                        android.util.Log.d("Checkmark", "THIS SMART MACHINE");
                        leftSkip.setVisibility(View.VISIBLE);
                        rightSkip.setVisibility(View.VISIBLE);
                        disconnectFromMachine.setVisibility(View.GONE);
                        connectToMachine.setVisibility(View.VISIBLE);
                        machineStatusColor.setBackgroundColor(Color.RED);
                        connectionStatus.setText("Not Connected");
                        DisconnectFromMachine();
                    } else {
                        ToggleCompletedDown();

                        // Moves after .5 second delay
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                SwipeExercise("RIGHT", 1);
                            }
                        }, 500);
                    }

                }

            }
        });

        // Add Set Click Listener
        addSet.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        android.widget.LinearLayout.LayoutParams.WRAP_CONTENT);
                params.gravity = Gravity.CENTER;

                // This copies the previous rep/weight values onto the new rep/weight
                int size = workout.getCurrentExercise().repList.size();
                int rep=0;
                int weight=0;

                if( size > 0) {
                    rep = workout.getCurrentExercise().repList.get(size - 1);
                    weight = workout.getCurrentExercise().weightList.get(size-1);
                }
                else {
                    rep = 8;
                    weight = 35;
                }
                AddSetToSetBar(workout.CURRENT_EXERCISE, workout.getCurrentExercise().repList.size(), rep, params);
                AddWeightToSetBar(workout.CURRENT_EXERCISE, workout.getCurrentExercise().weightList.size(), weight, params);
            }
        });

        // Add Set Click Listener
        deleteSet.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                DeleteSetFromSetBar();
            }
        });


        connectToMachine.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                Log.d("nfc SendIpToMachine", CurrentHost + " " + RecievedMachineId);
                //TODO: Send Ip TO Machines is Disabled
                //SendIpToMachine(CurrentHost, RecievedMachineId);

                currentRepBar.setVisibility(View.VISIBLE);
                leftSkip.setVisibility(View.INVISIBLE);
                rightSkip.setVisibility(View.INVISIBLE);
                connectToMachine.setVisibility(View.GONE);
                disconnectFromMachine.setVisibility(View.VISIBLE);
                machineStatusColor.setBackgroundColor(Color.GREEN);
                connectionStatus.setText("Connected");

                //Log.d("bench", workout.getCurrentExercise().getName());

                // Check if Current Machine is Bench Press
                if (workout.getCurrentExercise().getName().equals("Bench Press")) {
                    Log.d("bench", "Track Started");
                    //TODO: Mins Bench Press Tracking
                    //StartBenchPressTracking();


                } else {
                    Log.d("bench", "Track Not Started");
                    //TODO: Start Smart Machine is currently Disabled
                    //parseStartSmartSession();
                    BluetoothStartSmartSession();
                }


            }
        });


        updateRepButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //UpdateCurrentRepParse();
            }
        });


        incrementRep.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                dareps++;
                //setObject.put("reps", dareps);
                //setObject.saveInBackground();
                BluetoothUpdateCurrentRep(dareps);


            }
        });

        incrementSet.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dareps = 0;
                dasets++;
                //setObject = new ParseObject("sets");
                //setObject.put("setNumber", dasets);
                //setObject.put("reps", dareps);
                //setObject.put("userId", ParseUser.getCurrentUser().getObjectId());
                //setObject.put("machineId", machineIds.get(workout.getNames().get(workout.CURRENT_EXERCISE)));
                //setObject.saveInBackground();
                BluetoothStartSmartSession();
            }
        });

        disconnectFromMachine.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                connectionStatus.setText("Not Connected");
                currentRepBar.setVisibility(View.GONE);
                leftSkip.setVisibility(View.VISIBLE);
                rightSkip.setVisibility(View.VISIBLE);
                disconnectFromMachine.setVisibility(View.GONE);
                connectToMachine.setVisibility(View.VISIBLE);
                machineStatusColor.setBackgroundColor(Color.RED);


                if (benchRepper != null)
                    benchRepper.end();

                //TODO: Uncomment hen using client library
                /*
                try {

                   // client.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                */

                DisconnectFromMachine();

            }
        });


        registerIntentRecievers();


        // Bluetooth Shit
        //try {
          //  init();
           // write("Test");

        //} catch (IOException e) {
          //  e.printStackTrace();
        //}


    }

    private void init() throws IOException {
        BluetoothAdapter blueAdapter = BluetoothAdapter.getDefaultAdapter();
        if (blueAdapter != null) {
            if (blueAdapter.isEnabled()) {
                Set<BluetoothDevice> bondedDevices = blueAdapter.getBondedDevices();

                if (bondedDevices.size() > 0) {
                    BluetoothDevice device = (BluetoothDevice) bondedDevices.toArray()[0];
                    ParcelUuid[] uuids = device.getUuids();
                    BluetoothSocket socket = device.createRfcommSocketToServiceRecord(uuids[0].getUuid());
                    socket.connect();
                    outputStream = socket.getOutputStream();
                }

                Log.e("error", "No appropriate paired devices.");
            } else {
                Log.e("error", "Bluetooth is disabled.");
            }
        }
    }

    public void write(String s) throws IOException {

        // outputStream.write(s.getBytes());
        // Wrap the OutputStream with DataOutputStream
        DataOutputStream dOut = new DataOutputStream(outputStream);

        // Encode the string with UTF-8
        byte[] message = s.getBytes("UTF-8");

        // Send it out
        dOut.write(message, 0, message.length);

    }


    void registerIntentRecievers() {

        IntentFilter updateSetIntentFilter = new IntentFilter("UPDATE_CURRENT_SET");
        MyBroadCastReciever pushReceiver;
        pushReceiver = new MyBroadCastReciever() {
            public void onReceive(Context context, Intent intent) {
                Bundle extras = intent.getExtras();
                String message = extras != null ? extras.getString("com.parse.Data") : "";
                JSONObject jObject;
                try {
                    jObject = new JSONObject(message);
                    //Toast toast = Toast.makeText(context, "UPDATE_CURRENT_SET", Toast.LENGTH_SHORT);
                    //toast.show();

                    workout.CURRENT_SET_ID = jObject.get("objectId").toString();

                    Log.d("JSON Object Set Id", jObject.get("objectId").toString());

                    currentRepBar.setVisibility(View.VISIBLE);
                    leftSkip.setVisibility(View.INVISIBLE);
                    rightSkip.setVisibility(View.INVISIBLE);
                    connectToMachine.setVisibility(View.GONE);
                    disconnectFromMachine.setVisibility(View.VISIBLE);
                    machineStatusColor.setBackgroundColor(Color.parseColor("#2ECC71"));

                    parseStartSmartSession();
                    //StartExerciseSession();
                    //UpdateCurrentRep();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        registerReceiver(pushReceiver, updateSetIntentFilter);

        IntentFilter update_current_rep_intent_filter = new IntentFilter("UPDATE_CURRENT_REP");
        MyBroadCastReciever update_current_rep_broadcast_reciever;
        update_current_rep_broadcast_reciever = new MyBroadCastReciever() {
            public void onReceive(Context context, Intent intent) {
                Bundle extras = intent.getExtras();
                String message = extras != null ? extras.getString("com.parse.Data") : "";
                JSONObject jObject;
                try {
                    jObject = new JSONObject(message);
                    //Toast toast = Toast.makeText(context, "UPDATE_CURRENT_REP", Toast.LENGTH_SHORT);
                    //toast.show();

                    workout.CURRENT_SET_ID = jObject.get("objectId").toString();

                    //Log.d("JSON Object", jObject.toString());
                    //Log.d("get json object data", jObject.getJSONObject("data").toString());
                    Log.d("get json object ", jObject.get("objectId").toString());

                    UpdateCurrentRepParse();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        registerReceiver(update_current_rep_broadcast_reciever, update_current_rep_intent_filter);

    }

    void inflateVariables() {

        machineCompleted = (ImageView) findViewById(R.id.machineCompleted);
        addSet = (ImageView) findViewById(R.id.addSet);
        deleteSet = (ImageView) findViewById(R.id.deleteSet);
        machineName = (TextView) findViewById(R.id.machineName);
        exerciseSets = (TextView) findViewById(R.id.exerciseSets);
        currentRepBar = (TextView) findViewById(R.id.currentRepBar);
        noSmart = (TextView) findViewById(R.id.noSmartMachineFound);
        connectionStatus = (TextView) findViewById(R.id.connectionStatus);
        addText = (TextView) findViewById(R.id.addText);
        deleteText = (TextView) findViewById(R.id.deleteText);
        completeText = (TextView) findViewById(R.id.completeText);
        connectToMachine = (Button) findViewById(R.id.connectToMachine);
        updateRepButton = (Button) findViewById(R.id.updateRep);
        incrementRep = (Button) findViewById(R.id.incrementRep);
        incrementSet = (Button) findViewById(R.id.incrementSet);
        disconnectFromMachine = (Button) findViewById(R.id.disconnectFromMachine);
        setsLinearLayout = (LinearLayout) findViewById(R.id.setsLinearLayout);
        weightsLinearLayout = (LinearLayout) findViewById(R.id.weightsLinearLayout);
        smartExerciseConsole = (LinearLayout) findViewById(R.id.smartExerciseConsole);
        customExerciseConsole = (LinearLayout) findViewById(R.id.customExerciseConsole);
        setNumberLinearLayout = (LinearLayout) findViewById(R.id.setNumberLinearLayout);

        rightSkip = (ImageView) findViewById(R.id.rightSkip);
        leftSkip = (ImageView) findViewById(R.id.leftSkip);


    }

    /*
    void StartBenchPressTracking(){

        currentRepBar.setVisibility(View.GONE);

        Handler handler = new Handler();
        final int delay = 5000;
        incrementSet.performClick();


        final MachineProgressFragment fragment = (MachineProgressFragment) machineProgressPagerAdapter.instantiateItem(machineViewpager, machineViewpager.getCurrentItem());
        fragment.incrementSet();

        //Orientation Provider
        benchRepper = new BenchRepper(getApplicationContext())
        {
            @Override
            public void rep()
            {

                final Runnable r = new Runnable() {
                    public void run() {
                        Toast.makeText(getApplicationContext(),"RUN!",Toast.LENGTH_SHORT).show();
                        incrementSet.performClick();
                        fragment.incrementSet();
                    }
                };

                handler.removeCallbacksAndMessages(null);


                if( incAllowed ) {
                    repCount++;
                    Log.d("bench", "incremented" + String.valueOf(repCount));
                    fragment.incrementRep();

                    incrementRep.performClick();

                    incAllowed = false;


                    handler.postDelayed(r, delay);

                }


                final ScheduledExecutorService worker = Executors.newSingleThreadScheduledExecutor();
                Runnable runny = new Runnable() {
                    @Override
                    public void run() {
                        incAllowed = true;
                    }
                };
                worker.schedule(runny,1750, TimeUnit.MILLISECONDS);
            }
        };

        //initially 0
        repCount = 0;

        benchRepper.start();
    }
    */


    void SetupWorkoutFromObject(String objectId) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("UserWorkout");
        query.getInBackground(objectId, new GetCallback<ParseObject>() {
            public void done(ParseObject workoutQuery, ParseException e) {
                if (e == null) {

                    names = (ArrayList<String>) workoutQuery.get("machines");
                    sets = (ArrayList<String>) workoutQuery.get("sets");

                    MAX_PAGES = names.size();


                    // Set UI current exercise
                    machineName.setText(names.get(workout.CURRENT_EXERCISE));

                    // Create Workout Object
                    workout.workoutName = workoutQuery.getString("name");
                    for (int i = 0; i < MAX_PAGES; i++) {
                        workout.setIndexes.add(-1);
                        machineCompletedList.add(false);
                    }
                    workout.addExercises((ArrayList<String>) workoutQuery.get("machines"),
                            (ArrayList<String>) workoutQuery.get("sets"),
                            (ArrayList<String>) workoutQuery.get("weights"));

                    GenerateSetsList(workout.CURRENT_EXERCISE);
                    GenerateWeightsList(workout.CURRENT_EXERCISE);

                    machineProgressPagerAdapter = new MachineProgressPagerAdapter(getSupportFragmentManager(), StartWorkout.this, names, sets);

                    machineViewpager = (ViewPager) findViewById(R.id.machineViewPager);
                    machineViewpager.setAdapter(machineProgressPagerAdapter);
                    machineTabs = (SlidingTabLayout) findViewById(R.id.machineTabs);
                    machineTabs.setViewPager(machineViewpager);

                    //setting indicator and divider color
                    machineTabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {

                        @Override
                        public int getIndicatorColor(int position) {
                            return getResources().getColor(R.color.colorAccent);    //define any color in xml resources and set it here, I have used white
                        }
                    });


                    // When the fragment is changed, also change exercise information on the UI
                    // Change machine name, sets, reps, etc.
                    machineViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                        @Override
                        public void onPageScrolled(int i, float v, int i1) {

                        }

                        @Override
                        public void onPageSelected(int indexModifier) {
                            Log.d("onPageSelected", String.valueOf(indexModifier));

                            String nameOfMachine = names.get(indexModifier);

                            // If a smart exercise
                            // This checks the hard coded map of smart gym to see if name is inside map
                            SetExerciseConsole(nameOfMachine);


                            if (machineCompletedList.get(indexModifier) == false) {
                                machineCompleted.setImageResource(R.mipmap.unselectedcheckmark);
                                //machineState.setText("Incomplete");
                            } else {
                                machineCompleted.setImageResource(R.mipmap.selectedcheckmark);
                                //machineState.setText("Complete");
                            }

                            GenerateSetsList(indexModifier);
                            GenerateWeightsList(indexModifier);

                            workout.CURRENT_EXERCISE = indexModifier;

                        }

                        @Override
                        public void onPageScrollStateChanged(int i) {
                            //Log.d("onPageScrollStateChanged",String.valueOf(i));
                        }
                    });


                    SetExerciseConsole(workout.getCurrentExercise(0).getName());



                } else {
                    // something went wrong
                    Toast.makeText(
                            getApplicationContext(),
                            "ERROR WORKOUT",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    void SetExerciseConsole(String nameOfMachine){

        if (machineIds.get(nameOfMachine) != null) {
            customExerciseConsole.setVisibility(View.GONE);
            smartExerciseConsole.setVisibility(View.VISIBLE);
            machineName.setText(nameOfMachine);

        } else {
            customExerciseConsole.setVisibility(View.VISIBLE);
            smartExerciseConsole.setVisibility(View.GONE);

        }


    }


    void SetupWorkoutFromScratch() {

        MAX_PAGES = 0;


        // Set UI current exercise
        machineName.setText("Press Circle above to add exercise");

        workout.workoutName = "Custom Workout";
        workout.addExercises(new ArrayList<String>(), new ArrayList<String>(),
                new ArrayList<String>());

        GenerateSetsList(workout.CURRENT_EXERCISE);
        GenerateWeightsList(workout.CURRENT_EXERCISE);


        machineViewpager = (ViewPager) findViewById(R.id.machineViewPager);
        machineViewpager.setAdapter(new MachineProgressPagerAdapter(getSupportFragmentManager(), StartWorkout.this, names, sets));
        machineTabs = (SlidingTabLayout) findViewById(R.id.machineTabs);
        machineTabs.setViewPager(machineViewpager);

        //setting indicator and divider color
        machineTabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {

            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.colorAccent);    //define any color in xml resources and set it here, I have used white
            }
        });


        // Force Default Console of No Smart Machine
        customExerciseConsole.setVisibility(View.VISIBLE);
        smartExerciseConsole.setVisibility(View.GONE);


    }


    @Override
    protected void onResume() {
        super.onResume();

        /**
         * It's important, that the activity is in the foreground (resumed). Otherwise
         * an IllegalStateException is thrown.
         */
        setupForegroundDispatch(this, mNfcAdapter);
    }

    @Override
    protected void onPause() {
        /**
         * Call this before onPause, otherwise an IllegalArgumentException is thrown as well.
         */
        stopForegroundDispatch(this, mNfcAdapter);

        super.onPause();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        /**
         * This method gets called, when a new Intent gets associated with the current activity instance.
         * Instead of creating a new activity, onNewIntent will be called. For more information have a look
         * at the documentation.
         *
         * In our case this method gets called, when the user attaches a Tag to the device.
         */
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        String action = intent.getAction();
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            String type = intent.getType();

            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            new NdefReaderTask().execute(tag);
            if (MIME_TEXT_PLAIN.equals(type)) {


            } else {
                Log.d("NFC", "Wrong mime type: " + type);
            }
        } else if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)) {

            // In case we would still use the Tech Discovered Intent
            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            String[] techList = tag.getTechList();
            String searchedTech = Ndef.class.getName();

            for (String tech : techList) {
                if (searchedTech.equals(tech)) {
                    new NdefReaderTask().execute(tag);
                    break;
                }
            }
        }
    }

    void SendIpToMachine(String HOST, String machineId) {
        try {

            Log.d("SendIptoM NFC HOST", HOST);
            Log.d("SendIptoM NFC machienId", machineId);

            client = new Client(HOST, PORT, machineId);//START NEW CLIENT OBJECT

            Thread t = new Thread(client);//INITIATE NEW THREAD
            t.start();//START THREAD

        } catch (Exception noServer)//IF DIDNT CONNECT PRINT THAT THEY DIDNT
        {

            noServer.printStackTrace();
            Log.d("Catch", "The server might not be up at this time.");
            //System.out.println(noServer.getMessage().toString());
        }
    }


    // This will Skip to next exercise, or previous, or any depending on the modifier
    // Ex: indexModifier = 1 means next, indexModifier = -1 means previous
    // Direction string is for the programmer to understand
    void SwipeExercise(String Direction, int indexModifier) {


        // Check Bounds
        if (workout.CURRENT_EXERCISE + indexModifier != -1 && workout.CURRENT_EXERCISE + indexModifier < names.size()) {

            // Set new index
            workout.CURRENT_EXERCISE += indexModifier;

            // Swipe ViewPager Programmaticaly
            machineViewpager.setCurrentItem(workout.CURRENT_EXERCISE);

        }
    }

    void parseStartSmartSession() {

        ParseQuery<ParseObject> query = ParseQuery.getQuery("sets");
        query.getInBackground(workout.CURRENT_SET_ID, new GetCallback<ParseObject>() {
            public void done(ParseObject set, ParseException e) {
                if (e == null) {

                    Toast.makeText(getApplicationContext(), "RECIEVED SET ", Toast.LENGTH_SHORT).show();

                    // Increment Set Index
                    workout.incrementSetIndex();

                    // If we need to add another set ( more than the user planned )
                    if (workout.getSetIndex() == workout.getCurrentExercise().repList.size()) {
                        // Add set to Layout
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.WRAP_CONTENT,
                                android.widget.LinearLayout.LayoutParams.WRAP_CONTENT);


                        int tempIndex = workout.getCurrentExercise().weightList.size();
                        AddSetToSetBar(workout.CURRENT_EXERCISE, tempIndex, 0, params);
                        AddWeightToSetBar(workout.CURRENT_EXERCISE, tempIndex, workout.getCurrentExercise().weightList.get(tempIndex - 1), params);

                    }

                    //Current number of reps is now 0
                    workout.getCurrentExercise().repList.set(workout.getSetIndex(), 0);

                    // Shows Current Rep text and value
                    currentRepBar.setText("Current Rep: " + set.get("reps").toString());

                    // Color EditText Sets
                    // And Update Previous Sets
                    for (int i = 0; i < setsLinearLayout.getChildCount(); i++) {
                        EditText repEditText = (EditText) setsLinearLayout.getChildAt(i);
                        EditText weightEditText = (EditText) weightsLinearLayout.getChildAt(i);

                        if (i < workout.getSetIndex()) {
                            repEditText.setTextColor(Color.parseColor("#263238"));
                            repEditText.setText(String.valueOf(workout.getCurrentExercise().repList.get(i)));
                            weightEditText.setTextColor(Color.parseColor("#263238"));
                            weightEditText.setText(String.valueOf(workout.getCurrentExercise().weightList.get(i)));
                        } else if (i == workout.getSetIndex()) {
                            repEditText.setTextColor(Color.parseColor("#F44336"));
                            weightEditText.setTextColor(Color.parseColor("#F44336"));
                        } else {

                        }

                    }


                } else {
                    // something went wrong
                }
            }
        });


    }

    void UpdateCurrentRepParse() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("sets");
        query.getInBackground(workout.CURRENT_SET_ID, new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (e == null) {
                    currentRepBar.setText("Current Rep: " + object.get("reps").toString());
                    workout.getCurrentExercise().repList.set(workout.getSetIndex(), object.getInt("reps"));
                    // object will be your game score
                } else {
                    Log.d("ERROR updateCurrentRep", e.toString() + " " + workout.CURRENT_SET_ID);
                    // something went wrong
                }
            }
        });
    }


    void BluetoothInterpreter( String command ){

        //List<String> resultList = Arrays.asList(command.split(" "));

        if( command.equals("UPDATE_CURRENT_SET")){

        } else if (command.equals("UPDATE_CURRENT_REP")){

        }


    }

    void BluetoothStartSmartSession() {


        Toast.makeText(getApplicationContext(), "RECIEVED BLUTOOTH SET ", Toast.LENGTH_SHORT).show();

        // Increment Set Index
        workout.incrementSetIndex();

        // If we need to add another set ( more than the user planned )
        if (workout.getSetIndex() == workout.getCurrentExercise().repList.size()) {
            // Add set to Layout
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    android.widget.LinearLayout.LayoutParams.WRAP_CONTENT);


            int tempIndex = workout.getCurrentExercise().weightList.size();
            AddSetToSetBar(workout.CURRENT_EXERCISE, tempIndex, 0, params);
            AddWeightToSetBar(workout.CURRENT_EXERCISE, tempIndex, workout.getCurrentExercise().weightList.get(tempIndex - 1), params);

        }

        //Current number of reps is now 0
        workout.getCurrentExercise().repList.set(workout.getSetIndex(), 0);

        // Shows Current Rep text and value
        currentRepBar.setText("Current Rep: " + 0);

        // Color EditText Sets
        // And Update Previous Sets
        for (int i = 0; i < setsLinearLayout.getChildCount(); i++) {
            EditText repEditText = (EditText) setsLinearLayout.getChildAt(i);
            EditText weightEditText = (EditText) weightsLinearLayout.getChildAt(i);

            if (i < workout.getSetIndex()) {
                repEditText.setTextColor(Color.parseColor("#263238"));
                repEditText.setText(String.valueOf(workout.getCurrentExercise().repList.get(i)));
                weightEditText.setTextColor(Color.parseColor("#263238"));
                weightEditText.setText(String.valueOf(workout.getCurrentExercise().weightList.get(i)));
            } else if (i == workout.getSetIndex()) {
                repEditText.setTextColor(Color.parseColor("#F44336"));
                weightEditText.setTextColor(Color.parseColor("#F44336"));
            } else {

            }

        }


    }

    void BluetoothUpdateCurrentRep(int rep) {

        currentRepBar.setText("Current Rep: " + rep);
        workout.getCurrentExercise().repList.set(workout.getSetIndex(), rep);

    }


    void GenerateSetsList(int desiredExerciseIndex) {
        if (desiredExerciseIndex != -1) {

            // Clear Existing List
            setsLinearLayout.removeAllViews();
            setNumberLinearLayout.removeAllViews();

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    android.widget.LinearLayout.LayoutParams.WRAP_CONTENT);


            // Check if workout not from scratch
            if (workout.exerciseList.size() != 0) {
                ArrayList<Integer> sets = workout.exerciseList.get(desiredExerciseIndex).repList;
                //Log.d("sets size", String.valueOf(sets.size()));

                int i = 0;
                for (int rep : sets) {

                    AddSetToSetBar(desiredExerciseIndex, i, rep, params);
                    i++;

                }
            }

        }
    }

    void GenerateWeightsList(int desiredExerciseIndex) {
        if (desiredExerciseIndex != -1) {

            // Clear Existing List
            weightsLinearLayout.removeAllViews();

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    android.widget.LinearLayout.LayoutParams.WRAP_CONTENT);

            // Check if workout not from scratch
            if (workout.exerciseList.size() != 0) {
                ArrayList<Integer> weights = workout.exerciseList.get(desiredExerciseIndex).weightList;
                Log.d("weight size", String.valueOf(weights.size()));

                int i = 0;
                for (int weight : weights) {

                    AddWeightToSetBar(desiredExerciseIndex, i, weight, params);
                    i++;

                }
            }

        }
    }

    // What Exercise
    // What set position
    // What rep value
    // layout params
    void AddSetToSetBar(final int desiredExerciseIndex, final int setIndex, int rep, LinearLayout.LayoutParams params) {

        // TextView for indicating set #
        TextView setNumber = new TextView(this);
        setNumber.setGravity(Gravity.CENTER_HORIZONTAL);
        setNumber.setPadding(0, 5, 0, 0);
        setNumber.setLayoutParams(params);
        setNumber.setTextColor(Color.parseColor("#95A5A6"));
        setNumber.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 13);
        setNumber.setEms(3);
        setNumber.setText(String.valueOf(setIndex + 1));


        //if index is greater than size, then add
        if (setIndex >= workout.exerciseList.get(desiredExerciseIndex).repList.size())
            workout.exerciseList.get(desiredExerciseIndex).repList.add(rep);


        // EditText for Reps
        EditText repNumber = new EditText(this);
        repNumber.setLayoutParams(params);
        repNumber.setText(String.valueOf(rep));
        repNumber.setRawInputType(InputType.TYPE_CLASS_NUMBER);
        repNumber.setSingleLine(true);
        repNumber.setSelectAllOnFocus(true);
        repNumber.setTextColor(Color.WHITE);
        repNumber.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
        repNumber.setEms(2);
        repNumber.setFilters(new InputFilter[]{new InputFilter.LengthFilter(3)});
        repNumber.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {


            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {
                if (!s.toString().equals("")) {
                    String tempNum = s.toString();
                    Integer tempInt = Integer.parseInt(tempNum);
                    Log.d("s String value  ", tempNum);

                    workout.exerciseList.get(desiredExerciseIndex).repList.set(setIndex, tempInt);

                }
            }
        });

        setsLinearLayout.addView(repNumber);
        setNumberLinearLayout.addView(setNumber);
    }

    // What Exercise
    // What set position
    // What weight value
    // layout params
    void AddWeightToSetBar(final int desiredExerciseIndex, final int setIndex, int weight, LinearLayout.LayoutParams params) {


        //if index is greater than size, then add
        if (setIndex >= workout.exerciseList.get(desiredExerciseIndex).weightList.size())
            workout.exerciseList.get(desiredExerciseIndex).weightList.add(weight);


        EditText weightNumber = new EditText(this);
        weightNumber.setLayoutParams(params);
        weightNumber.setText(String.valueOf(weight));
        weightNumber.setRawInputType(InputType.TYPE_CLASS_NUMBER);
        weightNumber.setSingleLine(true);
        weightNumber.setSelectAllOnFocus(true);
        weightNumber.setTextColor(Color.WHITE);
        weightNumber.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
        weightNumber.setEms(2);
        weightNumber.setFilters(new InputFilter[]{new InputFilter.LengthFilter(3)});
        weightNumber.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void afterTextChanged(Editable s) {
                if (!s.toString().equals("")) {
                    String tempNum = s.toString();
                    Integer tempInt = Integer.parseInt(tempNum);
                    Log.d("s String value  ", tempNum);

                    workout.exerciseList.get(desiredExerciseIndex).weightList.set(setIndex, tempInt);

                }
            }
        });

        weightsLinearLayout.addView(weightNumber);
    }

    void DeleteSetFromSetBar() {
        int lastIndex = workout.getCurrentExercise().repList.size() - 1;

        // If it's not last remaining set ( we need atleast 1 set at all times )
        if (lastIndex != 0) {
            // Remove view from Layout
            setsLinearLayout.removeViewAt(lastIndex);
            setNumberLinearLayout.removeViewAt(lastIndex);
            weightsLinearLayout.removeViewAt(lastIndex);

            //Remove view from repList
            workout.getCurrentExercise().repList.remove(lastIndex);
            workout.getCurrentExercise().weightList.remove(lastIndex);
        } else {
            Toast.makeText(getApplicationContext(), "C'mon atleast do 1 set...", Toast.LENGTH_SHORT).show();
        }

    }


    void DisconnectFromMachine() {

        // Reset value
        workout.CURRENT_SET_ID = "";

        // Chop off remaining sets and weight if number of sets cut short of goal
        if (workout.getSetIndex() + 1 < workout.getCurrentExercise().repList.size()) {
            workout.getCurrentExercise().repList.subList(workout.getSetIndex() + 1, workout.getCurrentExercise().repList.size()).clear();
            workout.getCurrentExercise().weightList.subList(workout.getSetIndex() + 1, workout.getCurrentExercise().weightList.size()).clear();
        }

        // Complete the Exercise
        ToggleCompletedDown();
        ToggleCompletedDown();


    }

    /**
     * Code for Toggling Checkmarks
     * ********************************************************************************************************
     * ********************************************************************************************************
     */
    void ToggleCompletedDown() {
        machineCompleted.setImageResource(R.mipmap.selectedcheckmark);
        machineCompletedList.set(workout.CURRENT_EXERCISE, true);
        completedCount++;
        //machineState.setText("Complete");

        CheckIfWorkoutDone();
    }

    void ToggleCompletedUp() {
        machineCompleted.setImageResource(R.mipmap.unselectedcheckmark);
        machineCompletedList.set(workout.CURRENT_EXERCISE, false);
        completedCount--;
        //machineState.setText("Incomplete");

        CheckIfWorkoutDone();
    }

    void CheckIfWorkoutDone() {
        if (completedCount == MAX_PAGES) {
            Intent intent = new Intent(StartWorkout.this, WorkoutStats.class);
            intent.putExtra("workout", (Serializable) workout);
            intent.putExtra("workoutId", objectId);
            StartWorkout.this.startActivity(intent);
            workout.UploadToDatabase();
        }
    }

    /**
     * *******************************************************************************************************
     */


    /**
     * Toolbar Menu
     * *******************************************************************************************************
     * *******************************************************************************************************
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_start_workout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch(id){
            case R.id.action_settings:
                return true;
            case R.id.home:
                onBackPressed();
                break;
            case R.id.addNewExercise:
                AddNewExercise();
                break;
            case R.id.removeExercise:
                RemoveCurrentExercise();
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    void AddNewExercise() {

        FragmentManager fragmentManager = getFragmentManager();
        QuickAddExerciseDialog myDialog = new QuickAddExerciseDialog();
        myDialog.show(fragmentManager, "Add Exercise");

    }

    void RemoveCurrentExercise(){

        FragmentManager fragmentManager = getFragmentManager();
        QuickRemoveExerciseDialog myDialog = new QuickRemoveExerciseDialog();
        myDialog.show(fragmentManager, "Remove Current Exercise?");


    }

    /**
     * *******************************************************************************************************
     */





    /**
     * Overrided Dialog Functions
     * *******************************************************************************************************
     * *******************************************************************************************************
     */


    // For Quick Add Exercise Dialog
    @Override
    public void onCreateWorkoutName(String name) {
        String rep;
        String weight;

        // Set the initial rep and weight amount
        //TODO: Delete this. It seems uncessessary. Why not default to rep 8 weight 35?
        // If not Wokout from scratch
        if (workout.exerciseList.size() != 0) {
            int size = workout.getCurrentExercise().repList.size();
            rep = String.valueOf(workout.getCurrentExercise().repList.get(size - 1));
            weight = String.valueOf(workout.getCurrentExercise().weightList.get(size - 1));
        } else {
            rep = "8";
            weight = "35";

            // Workout is from scratch
            // So initialize the lists
            names = new ArrayList<String>();
            sets = new ArrayList<String>();

            // Set proper UI console if from scratch
            SetExerciseConsole(name);

        }


        // Add initial values to metric arrays
        names.add(name);
        sets.add(rep);

        // Add a new exercise as Not Completed to the completed list
        machineCompletedList.add(false);

        // Add 1 to number of pages
        MAX_PAGES++;

        // TODO: Explain this
        // Not sure why -1 but add to the list
        workout.setIndexes.add(-1);

        // Add new exercise to the Workout Object
        workout.addExercise(name, rep, weight);

        // If workout started from scratch
        // then generate UI right now since
        // the new exercise is the current exercise
        if (workout.exerciseList.size() == 1) {

            GenerateSetsList(workout.CURRENT_EXERCISE);
            GenerateWeightsList(workout.CURRENT_EXERCISE);

            addSet.setVisibility(View.VISIBLE);
            deleteSet.setVisibility(View.VISIBLE);
            machineCompleted.setVisibility(View.VISIBLE);
            rightSkip.setVisibility(View.VISIBLE);
            leftSkip.setVisibility(View.VISIBLE);
            addText.setVisibility(View.VISIBLE);
            deleteText.setVisibility(View.VISIBLE);
            completeText.setVisibility(View.VISIBLE);

        }


        // Set adapter with updated lists
        machineViewpager.setAdapter(new MachineProgressPagerAdapter(getSupportFragmentManager(), StartWorkout.this, names, sets));
        machineTabs.setViewPager(machineViewpager);


        // hide soft keyboard
        getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN );


    }


    // For Quick Delete Workout
    @Override
    public void onDialogRemoveCurrentExercise() {

        int currentIndex = workout.CURRENT_EXERCISE;

        // Add initial values to metric arrays
        names.remove(currentIndex);
        sets.remove(currentIndex);

        // Add a new exercise as Not Completed to the completed list
        machineCompletedList.remove(currentIndex);

        // Remove 1 to number of pages
        MAX_PAGES--;

        // TODO: Explain this
        // Not sure why -1 but add to the list
        workout.setIndexes.remove(currentIndex);

        // Add new exercise to the Workout Object
        //workout.addExercise(name, rep, weight);
        workout.RemoveExercise(currentIndex);

        // Set adapter with updated lists
        machineViewpager.setAdapter(new MachineProgressPagerAdapter(getSupportFragmentManager(), StartWorkout.this, names, sets));
        machineTabs.setViewPager(machineViewpager);

        // If first exercise is deleted
        int index=0;
        if( workout.CURRENT_EXERCISE == 0){
            index = 0;
        }
        // If exercise is deleted in between
        else if (workout.CURRENT_EXERCISE < MAX_PAGES){
            index = workout.CURRENT_EXERCISE;

        } else if( workout.CURRENT_EXERCISE == MAX_PAGES){
            index = workout.CURRENT_EXERCISE-1;
        }


        // Set Fragment For Machine
        machineViewpager.setCurrentItem(index);
        GenerateSetsList(index);
        GenerateWeightsList(index);

        workout.CURRENT_EXERCISE = index;

        Log.d("Max Pages", String.valueOf(MAX_PAGES));
        if(MAX_PAGES == 0){
            rightSkip.setVisibility(View.INVISIBLE);
            leftSkip.setVisibility(View.INVISIBLE);
            addSet.setVisibility(View.INVISIBLE);
            deleteSet.setVisibility(View.INVISIBLE);
            machineCompleted.setVisibility(View.INVISIBLE);
            addText.setVisibility(View.INVISIBLE);
            deleteText.setVisibility(View.INVISIBLE);
            completeText.setVisibility(View.INVISIBLE);
            customExerciseConsole.setVisibility(View.INVISIBLE);
            smartExerciseConsole.setVisibility(View.INVISIBLE);

        }

        //TODO: Check if workout completed
    }

    /**
     * *******************************************************************************************************
     */




    /**
     * NFC Code *********************************************************************************************
     * ********************************************************************************************************
     */

    /**
     * Background task for reading the data. Do not block the UI thread while reading.
     *
     * @author Ralf Wondratschek
     */
    private class NdefReaderTask extends AsyncTask<Tag, Void, String> {

        @Override
        protected String doInBackground(Tag... params) {
            Tag tag = params[0];

            Ndef ndef = Ndef.get(tag);
            if (ndef == null) {
                // NDEF is not supported by this Tag.
                Log.d("NFC READER TASK", "NDEF not supported");
                return null;
            }

            NdefMessage ndefMessage = ndef.getCachedNdefMessage();

            NdefRecord[] records = ndefMessage.getRecords();
            for (NdefRecord ndefRecord : records) {
                if (ndefRecord.getTnf() == NdefRecord.TNF_WELL_KNOWN && Arrays.equals(ndefRecord.getType(), NdefRecord.RTD_TEXT)) {
                    try {
                        return readText(ndefRecord);
                    } catch (UnsupportedEncodingException e) {
                        Log.e("NFC READER TASK", "Unsupported Encoding", e);
                    }
                } else {
                    Log.d("NFC READER TASK", "NOT IN IF STATMENT");
                }
            }

            return null;
        }

        private String readText(NdefRecord record) throws UnsupportedEncodingException {
        /*
         * See NFC forum specification for "Text Record Type Definition" at 3.2.1
         *
         * http://www.nfc-forum.org/specs/
         *
         * bit_7 defines encoding
         * bit_6 reserved for future use, must be 0
         * bit_5..0 length of IANA language code
         */
            Log.d("NFC READER TASK", "IN READ TEXT");

            byte[] payload = record.getPayload();

            // Get the Text Encoding
            String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16";

            // Get the Language Code
            int languageCodeLength = payload[0] & 0063;

            // String languageCode = new String(payload, 1, languageCodeLength, "US-ASCII");
            // e.g. "en"

            // Get the Text
            return new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1, textEncoding);
        }

        @Override
        protected void onPostExecute(String result) {
            //Log.d("NFC READER RESULT", result);
            if (result != null) {

                List<String> resultList = Arrays.asList(result.split(","));

                Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
                Log.d("NFC result IP HOST", resultList.get(2).toString());
                Log.d("NFC result MachineID", resultList.get(0).toString());

                CurrentHost = resultList.get(2).toString();
                RecievedMachineId = resultList.get(0).toString();

                // If not from scratch
                if (objectId != null) {

                    Log.d("NFC RECIEVED IP HOST", CurrentHost);
                    Log.d("NFC RECIEVED MachineID", RecievedMachineId);
                } else // If from scratch
                {
                    String exerciseName = resultList.get(1);
                    onCreateWorkoutName(exerciseName);
                }
                connectToMachine.performClick();
            }
        }

    }

    /**
     * @param activity The corresponding {@link Activity} requesting the foreground dispatch.
     * @param adapter  The {@link NfcAdapter} used for the foreground dispatch.
     */
    public static void setupForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        final Intent intent = new Intent(activity.getApplicationContext(), activity.getClass());
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        final PendingIntent pendingIntent = PendingIntent.getActivity(activity.getApplicationContext(), 0, intent, 0);

        IntentFilter[] filters = new IntentFilter[1];
        String[][] techList = new String[][]{};

        // Notice that this is the same filter as in our manifest.
        filters[0] = new IntentFilter();
        filters[0].addAction(NfcAdapter.ACTION_NDEF_DISCOVERED);
        filters[0].addCategory(Intent.CATEGORY_DEFAULT);
        try {
            filters[0].addDataType(MIME_TEXT_PLAIN);
        } catch (IntentFilter.MalformedMimeTypeException e) {
            throw new RuntimeException("Check your mime type.");
        }

        adapter.enableForegroundDispatch(activity, pendingIntent, filters, techList);
    }

    /**
     * @param activity The corresponding {@link BaseActivity} requesting to stop the foreground dispatch.
     * @param adapter  The {@link NfcAdapter} used for the foreground dispatch.
     */
    public static void stopForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        adapter.disableForegroundDispatch(activity);
    }


    /*********************************************************************************************************
     *********************************************************************************************************
     */

}
