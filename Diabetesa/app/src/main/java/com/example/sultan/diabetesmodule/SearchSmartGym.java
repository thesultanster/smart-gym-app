package com.example.sultan.diabetesmodule;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SearchSmartGym extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private RecyclerView recyclerView;
    private SearchSmartGymRecyclerAdapter adapter;
    private android.support.v7.widget.Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_smart_gym);

        // Set Toolbar
        setToolbar();

        recyclerView = (RecyclerView) findViewById(R.id.searchSmartGymRecyclerView);
        adapter = new SearchSmartGymRecyclerAdapter(SearchSmartGym.this, new ArrayList<SearchSmartGymRecyclerInfo>());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(SearchSmartGym.this));


        // Query All Smart Gyms
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Location");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> gyms, ParseException e) {

                // If no errors
                if (e == null) {
                    Toast.makeText( getApplicationContext(),  String.valueOf(gyms.size()),  Toast.LENGTH_SHORT).show();

                    // For all gyms, add each gym to recyclerview
                    for (ParseObject gym : gyms) {
                        adapter.addRow(new SearchSmartGymRecyclerInfo(gym));
                    }

                } else {
                    // There is an Error
                    Toast.makeText( getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        
    }


    // Toolbar Code ===================================================================================================
    //=================================================================================================================


    void setToolbar(){

        CollapsingToolbarLayout collapsingToolbarLayout =  (android.support.design.widget.CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);


        // ToolBar Setup
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.parallax_toolbar);
        setSupportActionBar(toolbar);

        // Allocate Square for Home
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // Make Home A Back Button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        // Implement Back press
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // Add a title
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        collapsingToolbarLayout.setTitle("Find Smart Gyms");
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search_smart_gym, menu);

        // Associate searchable configuration with the SearchView
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(this);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
    //=================================================================================================================





    // Recycler Adapter and View Holder ====================================================================================================
    //======================================================================================================================================

    // the class contains my custom viewholder; MyViewHolder, and this class will return MyViewHolder as well
    public static class SearchSmartGymRecyclerAdapter extends RecyclerView.Adapter<SearchSmartGymRecyclerAdapter.MyViewHolder> {

        // emptyList takes care of null pointer exception
        List<SearchSmartGymRecyclerInfo> data = Collections.emptyList();
        LayoutInflater inflator;

        // TODO: Try view.getCotenxt instead
        Context context;

        public SearchSmartGymRecyclerAdapter(Context context, List<SearchSmartGymRecyclerInfo> data) {
            this.context = context;
            inflator = LayoutInflater.from(context);
            this.data = data;
        }

        // Implemented my own addRow
        public void addRow(SearchSmartGymRecyclerInfo row){
            data.add(row);
            notifyItemInserted(getItemCount()-1);
        }

        // Implemented my own delete row
        public void deleteRow(int position) {
            data.remove(position);
            notifyItemRemoved(position);
        }

        // Called when the recycler view needs to create a new row
        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            final View view = inflator.inflate(R.layout.row_search_smart_gym, parent, false);
            MyViewHolder holder = new MyViewHolder(view, new MyViewHolder.MyViewHolderClicks() {
                public void RowClickGym(View caller, int position) {

                    Intent intent = new Intent(context, ViewSmartGym.class);

                    // Add arguments
                    //intent.putExtra("selectedId", data.get(position).getParseObjectId());

                    // Start Activity
                    view.getContext().startActivity(intent);
                }
            });

            return holder;
        }

        // Setting up the data for each row
        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {

            // This gives us current information list object
            SearchSmartGymRecyclerInfo current = data.get(position);

            // Bind Values
            holder.gymName.setText(current.getGymName());
            holder.gymAddress.setText(current.getGymAddress());
            holder.gymCity.setText(current.getGymCity());
            holder.gymState.setText(current.getGymState());


        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        // Created my custom view holder
        public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            TextView gymName;
            TextView gymAddress;
            TextView gymCity;
            TextView gymState;
            LinearLayout layout;


            public MyViewHolderClicks mListener;

            // itemView will be my own custom layout View of the row
            public MyViewHolder(View itemView, MyViewHolderClicks listener) {
                super(itemView);

                // Listener for view clicks
                mListener = listener;

                //Link the objects
                gymName = (TextView) itemView.findViewById(R.id.gymName);
                gymAddress = (TextView) itemView.findViewById(R.id.gymAddress);
                gymState = (TextView) itemView.findViewById(R.id.gymState);
                gymCity = (TextView) itemView.findViewById(R.id.gymCity);
                layout = (LinearLayout) itemView.findViewById(R.id.layout_search_smart_gym);

                gymName.setOnClickListener(this);
                layout.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                switch(v.getId()) {

                    // If click Gym Name
                    case R.id.gymName:
                        mListener.RowClickGym(v, getAdapterPosition());
                        break;
                    // Otherwise if click row
                    default:
                        mListener.RowClickGym(v, getAdapterPosition());
                        break;
                }
            }

            // Interface to handle click event
            public  interface MyViewHolderClicks {
                public void RowClickGym(View caller, int position);
            }
        }
    }
    //======================================================================================================================================




    // Recycler Info Object =====================================================================================
    //===========================================================================================================

    public static class SearchSmartGymRecyclerInfo {

        ParseObject gym;

        public SearchSmartGymRecyclerInfo() { super(); }

        public SearchSmartGymRecyclerInfo(ParseObject gym) { super(); this.gym = gym; }

        public String getGymName() {
            return gym.get("Name").toString();
        }

        public String getGymState() {
            return gym.get("State").toString();
        }

        public String getGymCity() {
            return gym.get("City").toString();
        }

        public String getGymAddress() {
            return gym.get("Address").toString();
        }

        public void setGymName(String name) { gym.put("Name",name); }

        public String getParseObjectId() {
            return gym.getObjectId();
        }

    }


    //==========================================================================================================


}
