package com.example.sultan.diabetesmodule;

import android.widget.TextView;

public class SetRecyclerInfo {

    private String setAmount;
    private String repsAmount;

    public SetRecyclerInfo( String setAmount, String repsAmount) {
        super();
        this.setAmount = setAmount;
        this.repsAmount = repsAmount;
    }

    public SetRecyclerInfo( ) {
        super();

    }
    public String getSet() {
        return setAmount;
    }
    public void setSet(String setAmount) {
        this.setAmount = setAmount;
    }

    public String getRep() {
        return repsAmount;
    }
    public void setRep(String repsAmount) {
        this.repsAmount = repsAmount;
    }


}
